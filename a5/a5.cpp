/*
 * CS830 Assignment 5
 * Sai Lekyang
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_set>
#include <list>
#include <vector>
#include <functional>
#include <math.h> 
#include <algorithm>

using namespace std;

char DEBUG = 0;
int BRANCHING = 0; 


    
char walksat(list <unordered_set <int>>clauses, vector <int> solution, int var, int depth, int var_cnt, int cl_cnt){
    // for 1 to maxTries
    //   assign all var randomly
    //   from 1 to maxFlips
    //      randomly choose an unsatisfied clause
    //      if one or more of c's var can be flipped while not break
    //          randomly choose among those
    //      else
    //          with prob of 0.5 randomly choose one of c's variables
    //          else randomly choose among those of c's vars that minimize breaks
    //      flip the variable
    //      if formula satisfied, done

    int max_tries = 0;
    int total_flips = 0;
    
    while(1000 >= ++max_tries){

        // Assign variables randomly
        for (int i = 0; i < solution.size(); ++i){
            if (rand() % 2 == 1)
                solution[i] = 1;
            else 
                solution[i] = -1;
        }    

        int max_flips = 0;
        while (10000 >= ++max_flips){
            ++total_flips;
            // Check if statisfied and if not add to unsats vector

            vector<unordered_set<int>> unsats;
            vector<unordered_set<int>> sats;

            for (unordered_set<int> clause : clauses){
                int invalid = 0;
                for (auto it = clause.begin(); it != clause.end(); ++it) {
                    int v = *it;
                    int b = solution[abs(v) - 1]; // sign
                    if (v * b > 0){
                        sats.push_back(clause);
                        break;
                    }
                    ++invalid;
                }

                if (invalid == clause.size()){
                    unsats.push_back(clause);
                }

            }

            if (unsats.size() == 0){
                
                cout << "s cnf 1 " << var_cnt << " " << cl_cnt << endl;

                for( int i = 0; i < solution.size(); ++i){
                    int t = solution[i];
                    if (t == 0)
                        cout << "v " << (i + 1) << endl;
                    else 
                        cout << "v " << (i + 1) * t << endl;
                }

                cout << max_tries << " tries." << endl;
                cout << total_flips << " total flips.";
                return 1;
            }

            // Randomly choose an unstat clause
            unordered_set<int> rand_clause = unsats[rand() % unsats.size()];

            // If one or more of c's var can be flipped while not break
            // randomly choose among those
            vector<int> cvars;
            vector<int> breaks;

            vector<int> validvars;

            for (auto it = rand_clause.begin(); it != rand_clause.end(); ++it) {
                int var = abs(*it);
                int tmp = solution[var - 1];
                // Flip
                solution[var - 1] = tmp * -1;

                int valid = 0;
                cvars.push_back(var);

                for (unordered_set<int> clause : sats){
                    // Going through all the valid clauses to check for breaks.
                    int invalid = 0;

                    // Check to see if that clause contains the variable
                    if (clause.find(tmp) != clause.end() || clause.find(-tmp) != clause.end()){
                        for (auto ct = clause.begin(); ct != clause.end(); ++ct) {
                            int v = *ct;
                            int b = solution[abs(v) - 1]; // sign
                            if (v * b > 0){
                                break;
                            }
                            ++invalid;
                        }
                    }

                    if (invalid == clause.size()){
                        // Break
                        solution[abs(*it) - 1] = tmp;
                        break;
                    } 
                    
                    solution[abs(*it) - 1] = tmp;

                
                    ++valid;
                }

                breaks.push_back(valid);

                if (valid == sats.size()){
                    validvars.push_back(var);
                } 
            }

            int flip = 0;

            if (validvars.size() != 0){
                flip = validvars[ rand()% validvars.size() ];
            } else {
                if (rand() % 2 == 1){
                    // with prob of 0.5 randomly choose one of c's variables
                    flip = cvars[ rand()% cvars.size() ];
                } else {
                    // else randomly choose among those of c's vars that minimize breaks
                    int max = 0;
                    int indx = 0;
                    for (int i = 0; i < breaks.size(); ++i){
                        if (max < breaks[i]){
                            max = breaks[i];
                            indx = i;
                        }
                    }

                    flip = cvars[ indx ];

                }
            }

            solution[flip - 1] = solution[flip - 1] * -1;

        }


        if (max_tries >= 75){
            
            cout << "s cnf -1 " << var_cnt << " " << cl_cnt << endl;

            for( int i = 0; i < solution.size(); ++i){
                int t = solution[i];
                if (t == 0)
                    cout << "v " << (i + 1) << endl;
                else 
                    cout << "v " << (i + 1) * t << endl;
            }

            cout << max_tries << " tries." << endl;
            cout << total_flips << " total flips.";
            return 0;
        }
    }

    cout << "s cnf 0 " << var_cnt << " " << cl_cnt << endl;
    cout << max_tries << " tries." << endl;
    cout << total_flips << " total flips.";

    return 0;
}

char dll(list <unordered_set <int>>clauses, vector <int> solution, int var, int depth, int var_cnt, int cl_cnt){
    if (DEBUG)
        cout << "________________" << depth << "________________" << endl;

    ++BRANCHING;
    if (var != 0){
        if (var > 0){
            solution[abs(var) - 1] = 1;
        } else {
            solution[abs(var) - 1] = -1;
        }

        // If it's a positive var (true), then need to remove clauses with with
        // that var. And remove all the negative variable from all the clauses.
        // If negative, do opposite

        int index = clauses.size();
        bool erase = false;
        list <unordered_set <int>>::iterator tmp;
        list <unordered_set <int>>::iterator it = clauses.end();

        while (index > 0 && clauses.size() > 0){
            --index;
            --it;
            if (erase){
                clauses.erase(tmp);
                erase = false;
            }

            if ((*it).find(var) != (*it).end()){
                if (DEBUG){ 
                    cout << "Erasing whole clause at index... " << index << endl;
                }
                erase = true;
                tmp = it;
                // (*it).clear();
                // (*it).insert(0);

            } else if ((*it).find(-var) != (*it).end()){
                if (DEBUG) cout << "Erasing variable " << -var << " at index... " << index << endl;
                if ((*it).size() == 1){
                    if (DEBUG) cout << "Empty!" << endl;

                    return 0;
                }             
                
                (*it).erase(-var);   
            }
        }

        if (erase){
            clauses.erase(tmp);
        }


        if (DEBUG){
            cout << endl;
            for (unordered_set <int> c : clauses){
                for (int i : c){
                    cout << i << " ";
                }
                cout << endl;
            } 
            cout << endl;

            for (int i : solution){
                cout << i << " ";
            } cout << endl;
        }
    }

    if (clauses.size() == 0){
        cout << "s cnf 1 " << var_cnt << " " << cl_cnt << endl;
        for( int i = 0; i < solution.size(); ++i){
            int t = solution[i];
            if (t == 0)
                cout << "v " << (i + 1) << endl;
            else 
                cout << "v " << (i + 1) * t << endl;
        }
        return 1;
    }

    int smallest = 0xFF;
    int small_indx = -1;

    int index = 0;
    for (unordered_set <int> c : clauses){
        int size = c.size();
        if (size == 0){
            if (DEBUG) cout << "Empty" << endl;
            // Empty clause!
            return 0;
        }
        if (c.find(0) != c.end()){
            ++index;
            continue;
        }
        if (c.size() < smallest){
            smallest = c.size();
            small_indx = index;
        }
        ++index;
    }

    auto cur = clauses.begin();
    advance(cur, small_indx);

    if (DEBUG){
        cout << "Smallest... " << endl;
        for (int c : *cur){
            cout << c << " ";
        }
        cout  << endl;
    }
    


    for (int c : *cur){
        if (DEBUG){
            cout << "Branch... " << c << endl;
        }
        
        if (solution[abs(c) - 1] == 0){
            char r = dll(clauses, solution, c, depth + 1, var_cnt, cl_cnt);
            if (r == 1){
                return 1;
            }
            r = dll(clauses, solution, -c, depth + 1, var_cnt, cl_cnt);
            if (r == 1){
                return 1;
            }
            
        }
        
    }
    return 0;
}

int main(int argc, char** argv){
    string line;
    bool walksat_flag = false;
    bool read = false;
    int var_cnt = 0;
    int cl_cnt  = 0;
    list <unordered_set <int>> clauses;
    vector <int> solution;
 
    for (int i = 0; i < argc; ++i){
        string input = argv[i];
        if (input == "-w" ){
            walksat_flag = true;
        } 

        if (input == "-debug" ){
            DEBUG = 1;
        } 
        
    } 

    while (getline(cin,line)){
        stringstream ss(line);
        string word;
        ss >> word;

        if (read){
            unordered_set <int> c;
            while(!ss.eof()){
                if (word != "0"){
                    c.insert(stoi(word));
                } else {
                    clauses.push_back(c);
                    c.clear();
                }
                ss >> word;
            } 
            if (c.size() > 0)
                clauses.push_back(c);
        } else if (word == "p") {
            ss >> word; // "cnf"
            ss >> word;
            var_cnt = stoi(word);
            ss >> word;
            cl_cnt = stoi(word);
            read = true;
        }
    }

    for (int i = 0; i < var_cnt; ++i){
        solution.push_back(0);
    }

    
    if(DEBUG){
        cout << "Starting clauses..." << endl;
        for (unordered_set <int> c : clauses){
            for (int i : c){
                cout << i << " ";
            }
            cout << endl;
        } 
        cout << endl;

    }




    int r;
    if (walksat_flag)
        r = walksat(clauses, solution, 0, 0, var_cnt, cl_cnt);
    else {
        r = dll(clauses, solution, 0, 0, var_cnt, cl_cnt);

        if (r == 0){
            cout << "s cnf 0 " << var_cnt << " " << cl_cnt << endl;
        }
        
        cout << BRANCHING << " branching nodes explored.";

    }

    return 0;
}