#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <stack>
#include <list>
#include <queue>
#include <functional>

using namespace std;
bool DEBUG = false;
int HEURISTIC_TYPE;
int GRID_R;
int GRID_C;
int N_GEN = 0;
int N_EXP = 0;
int MAX_CHARGE;
int DUST;
int ID = 0;
bool BATTERY_FLAG = false;
char ORDER[6] = {'N', 'E', 'S', 'W', 'V', 'R'}; 


struct Node{
    string grid;
    int x, y, vac, cost, charge, f;
    bool overdust;
    bool overcharge;
    Node* parent;
    char move;
    int id;
    Node(string grid, int x, int y): 
        grid(grid), x(x), y(y), overdust(false), overcharge(false), parent(NULL), move(0){}
};

void d1(char c, Node* n){
    cout << c << " id:" << n->id << " mv:" << n->move << " vc:" << n->vac << " ch:" << n->charge << " c: " << n->cost << " f: " << n->f << " ";

    stack<char>path;
    Node* t = n;
    while(t->parent != NULL){
        path.push(t->move);
        t = t->parent;
    }
    while(!path.empty()){
        cout << path.top();
        path.pop();
    }
    cout << endl;

    cout << n->grid << endl;
}

void d2(string s){
    for (int i = 0; i < GRID_C * GRID_R; ++i){
        cout << s[i];
        if ((i + 1) % GRID_C == 0){
            cout << endl;
        }
    }
}

void debug(Node* cur){

    if (cur->parent){
        d1('P', cur->parent);
        d2(cur->parent->grid);
    }

    d1('C', cur);
    d2(cur->grid);
    cout << endl;
}

// 0,0 is top left corner
int get1D(int x, int y){
    return x + GRID_C * y;
}

bool checkGoal(Node* cur){
    Node* t = cur;

    if (DUST != t->vac){
        return false;
    }

    stack<char>path;
    
    while(t->parent != NULL){
        path.push(t->move);
        t = t->parent;
    }
    while(!path.empty()){
        cout << path.top() << endl;
        path.pop();
    }
    return true;
}

string getState(string state, int charge){
    if (BATTERY_FLAG){
        return state + to_string(charge);
    } 
    return state;
}

Node* getChild(Node* p, char dir, unordered_map<string, Node*>& closed_list, unordered_map<string, Node*>* open_set){
    int x = p->x;
    int y = p->y;
    int prev1D = get1D(x, y);

    // If no battery flag, force vaccum if over dust
    if (!BATTERY_FLAG && p->overdust && dir != 'V'){
        if(DEBUG) cout << "Over dust, need to vac" << endl;
        return NULL;
    }

    // Checking if there's enough juice
    if (BATTERY_FLAG && dir != 'R' && p->charge == 0){
        if(DEBUG) cout << "Low juice" << endl;
        return NULL;
    }

    // Check if move is possible
    if (dir == 'N'){
        if (p->y == 0){
            if(DEBUG) cout << "Cannot N" << endl;
            return NULL;
        }
        --y;
    } else if (dir == 'E'){
        if (p->x + 1 == GRID_C){
             if(DEBUG) cout << "Cannot E" << endl;
            return NULL;
        }
        ++x;
    } else if (dir == 'S'){
        if (p->y + 1 == GRID_R){
            if(DEBUG) cout << "Cannot S" << endl;
            return NULL;
        }
        ++y;
    } else if (dir == 'W'){
        if (p->x == 0){
            if(DEBUG) cout << "Cannot W" << endl;
            return NULL;
        }
        --x;
    } else if (dir == 'V'){
        if (!p->overdust){
            if(DEBUG) cout << "Cannot V" << endl;
            return NULL;
        }
    } else if (dir == 'R'){
        if (!BATTERY_FLAG || !p->overcharge || p->charge == MAX_CHARGE){
            if(DEBUG) cout << "Cannot R" << endl;
            return NULL;
        }
    }

    int next1D = get1D(x, y);
    
    // Obstacle at next step
    if( p->grid[next1D] == '#'){
         if(DEBUG) cout << "Obs next step " <<  dir << endl;
        return NULL;
    }

    // Generate next grid state
   
    string childState = p->grid;
    childState[next1D] = '@';
    bool overdust = false;
    bool overcharge = false;
    int charge = p->charge;
    int vac = p->vac;

    if (dir == 'V'){
        --charge;
        ++vac;
    } else if (dir =='R'){
        charge = MAX_CHARGE;
        // childState[next1D] = 'R';
        overcharge = true;
    } else {
        if (p->overdust){
            childState[prev1D] = '*';
        } else if (p->overcharge){
            childState[prev1D] = ':';
        } else {
            childState[prev1D] = '_';        
        }

        if (p->grid[next1D] == '*'){
            overdust = true;
            childState[next1D] = '!';
        } else if (p->grid[next1D] == ':'){
            overcharge = true;
            // childState[next1D] = '+';
        }

        if (BATTERY_FLAG){
            --charge;
            if (charge == 0){
                childState[next1D] = 'x';
            }
        }
    }
    ++N_GEN;

    if (open_set != NULL){
        unordered_map<string, Node*>::const_iterator it = (*open_set).find(getState(childState, charge));
        // cout << "checking open list for " << getState(childState, charge) << endl;
        // cout << (*open_set).size() << endl;


        if ( it != (*open_set).end() ){
            //cout << "already in open list" << endl;
            //cout << getState(childState, charge) << endl;

            if(DEBUG) {
            }
            return it->second;
        }
    }


    if (closed_list.count(getState(childState, charge)) > 0) {
        if(DEBUG) {
            Node* dup = closed_list[getState(childState, charge)];
            cout << "Already in hash " << dir << " " << getState(childState, charge) << " id:" << dup->id << endl;
        }
        return NULL;
    }
        
    Node* child = new Node(childState, x, y);
    child->vac = vac;
    child->parent = p;
    child->move = dir;
    child->cost =  p->cost + 1;
    child->id = ID++;
    child->charge =  charge;
    child->overdust = overdust;
    child->overcharge = overcharge;
    return child;
}

class compareSteps {
public:
    bool operator()(Node* n1, Node* n2) {
        return n1->cost > n2->cost;
    }
};

void algoUniformCost(Node* root){
    priority_queue<Node*, vector<Node*>, compareSteps> open_list;
    unordered_map<string, Node*> closed_list;
    open_list.push(root);
    closed_list[getState(root->grid, root->charge)] = root; 

    while(!open_list.empty()){
        Node* cur = open_list.top();
        ++N_EXP;

        if (DEBUG) debug(cur);
        if (checkGoal(cur)){
            return;
        }

        open_list.pop();
        for(char dir : ORDER){
            Node* n = getChild(cur, dir, closed_list, NULL);
            if(n != NULL){
                open_list.push(n);
                closed_list[getState(n->grid, n->charge)] = n; 
            }
        }
    }
    cout << "No solution found." << endl;
}

void algoDFSid(Node* root){
    int max_depth = 0;
    int last_size = 0;
    int c = 0;
    
    while(true){

        list<Node*> open_list;
        unordered_map<string, Node*> closed_list;
        open_list.push_front(root);
        closed_list[getState(root->grid, root->charge)] = root;
        stack<Node*> path;
        path.push(root);
        int expansion = 0;
        
        while (!open_list.empty()){
            Node* cur = open_list.front();
            open_list.pop_front();

            while(!path.empty() && path.top()->cost >= cur->cost){
                Node* t = path.top();
                closed_list.erase(getState(t->grid, t->charge));
                path.pop();
            }


            path.push(cur);

            ++N_EXP;
            ++expansion;

            if (DEBUG) debug(cur);
            if (checkGoal(cur)){
                return;
            }

            int children = 0;

            if (cur->cost <= max_depth){
                for(char dir : ORDER){
                    Node* n = getChild(cur, dir, closed_list, NULL);
                    if(n != NULL){
                        ++children;
                        open_list.push_front(n);
                        closed_list[getState(cur->grid, cur->charge)] = cur;
                    }
                }
            }

            // Dead end
            if (children == 0){
                --N_EXP;
                closed_list.erase(getState(cur->grid, cur->charge));
                path.pop();
            } 
        }

        if (last_size == expansion){
                break;
        }else{
            last_size = expansion;
        }
 
        ++max_depth;
    }

    cout << "No solution found." << endl;
}

void algoDFS(Node* root){
    list<Node*> open_list;
    unordered_map<string, Node*> closed_list;
    open_list.push_front(root);
    closed_list[getState(root->grid, root->charge)] = root; 
    
    while (!open_list.empty()){
        Node* cur = open_list.front();
        open_list.pop_front();
        ++N_EXP;

        if (DEBUG) debug(cur);
        if (checkGoal(cur)){
            return;
        }

        int children = 0;

        for(char dir : ORDER){
            Node* n = getChild(cur, dir, closed_list, NULL);
            if(n != NULL){
                ++children;
                open_list.push_front(n);
            }
        }

        // Dead end
        if (children == 0){
            closed_list.erase(getState(cur->grid, cur->charge));
        } else {
            closed_list[getState(cur->grid, cur->charge)] = cur;
        }
    }
    cout << "No solution found." << endl;
}

int getHeuristic(Node* n, int h){
    if (h == 0){
        return 1;
    } else if (h == 1){
        // This h will be dust remaining
        return DUST - n->vac;
    } else if (h == 2){
        // Add up the manhatten distance to all the dust
        int sum = 0;
        int dust = 0;
        int i;
        for (i = 0; i < GRID_R * GRID_C; ++i){
            if (n->grid[i] == '*'){
                sum += abs(n->x - ((i + 1) % GRID_C)) + abs(n->y - ((i + 1) / GRID_C));
                ++dust;
            }
        }
        if (dust == 0)
            return 0;
        sum = sum / dust;
        return sum;
    } else if (h == 3){
        // Add up the manhatten distance to all the dust
        int sum = 0;
        int dist_from_c = 0;
        int dust = 0;
        int closet_c = 2147483647;;
        for (int i = 0; i < GRID_R * GRID_C; ++i){
            if (n->grid[i] == '*'){
                sum += abs(n->x - ((i + 1) % GRID_C)) + abs(n->y - ((i + 1) / GRID_C));
                ++dust;
            } else if (n->grid[i] == ':'){
                dist_from_c = abs(n->x - (i % GRID_C)) + abs(n->y - (i / GRID_C));
                if (dist_from_c < closet_c){
                    closet_c = dist_from_c;
                }
            }
        }
        if (dust == 0)
            return 0;
        sum = sum / dust;

        if(!BATTERY_FLAG)
            return sum;

        return (sum + abs((n->charge) - 2*closet_c)/(n->charge + 1));
 
    }
    return 0;
}

class compareF{
public:
    bool operator()(Node* n1, Node* n2) {
        return n1->f > n2->f;
    }
};

void algoAStar(Node* root, int h){
    priority_queue<Node*, vector<Node*>, compareF> open_list;
    unordered_map<string, Node*> open_set;
    unordered_map<string, Node*> closed_list;

    open_list.push(root);
    open_set[root->grid] = root;
    root->f = getHeuristic(root, h);

    while(!open_list.empty()){
        Node* cur = open_list.top();
        ++N_EXP;

        if (DEBUG) debug(cur);
        if (checkGoal(cur)){
            return;
        }

        open_list.pop();
        open_set.erase(getState(cur->grid, cur->charge));
        closed_list[getState(cur->grid, cur->charge)] = cur; 

        for(char dir : ORDER){
            Node* n = getChild(cur, dir, closed_list, &open_set);
            if(n != NULL){
                // Distance from start to neighbor
                int d = cur->cost + 1;

                if (open_set.count(getState(n->grid, n->charge)) == 0){
                    // New node
                    n->f = n->cost + getHeuristic(n, h);
                    open_list.push(n);
                    open_set[getState(n->grid, n->charge)] = n;
                } else{
                    if(d < n->cost){
                        // Found a better path to exisiting neighbor
                        n->f = n->cost + getHeuristic(n, h);
                        n->parent = cur;
                        open_list.push(n);
                    } else {

                    }
                }
            }
        }
    }
    cout << "No solution found." << endl;
}

int algoAStartidSearch(unordered_map<string, Node*>& path, Node* n, int bound, int h){
    int f = n->cost + getHeuristic(n, h);
    if (checkGoal(n)){
        return 0;
    }

    if (f > bound){
        return f;
    }

    ++N_EXP;
    int min = 2147483647;

    for(char dir : ORDER){
        Node* c = getChild(n, dir, path, NULL);
        if (c != NULL){
            if(path.count(getState(c->grid, c->charge)) == 0){  
                c->f = n->cost + getHeuristic(c, h);
                // Child not in path
                path[getState(c->grid, c->charge)] = c; 
                int t = algoAStartidSearch(path, c, bound, h);
                if (t == 0){
                    return 0;
                }
                if (t < min)
                    min = t;
                path.erase(getState(c->grid, c->charge));
            } else {
 
            }
        }
    }
    return min;
}

void algoAStarid(Node* root, int h){
    int bound = getHeuristic(root, h);
    
    while(true){
        unordered_map<string, Node*> path;
        path[getState(root->grid, root->charge)] = root; 
        int t = algoAStartidSearch(path, root, bound, h);
        if (t == 0){
            return;
        } else if (t == 2147483647){
            cout << "No solution found." << endl;
            return;
        }
        bound = t;
    }
    return;
}

void algoGreedy(Node* root, int h){
priority_queue<Node*, vector<Node*>, compareF> open_list;
    unordered_map<string, Node*> open_set;
    unordered_map<string, Node*> closed_list;

    open_list.push(root);
    open_set[root->grid] = root;
    root->f = getHeuristic(root, h);

    while(!open_list.empty()){
        Node* cur = open_list.top();
        ++N_EXP;

        if (DEBUG) debug(cur);
        if (checkGoal(cur)){
            return;
        }

        open_list.pop();
        open_set.erase(getState(cur->grid, cur->charge));
        closed_list[getState(cur->grid, cur->charge)] = cur; 

        for(char dir : ORDER){
            Node* n = getChild(cur, dir, closed_list, &open_set);
            if(n != NULL){
                // Distance from start to neighbor
                int d = cur->cost + 1;

                if (open_set.count(getState(n->grid, n->charge)) == 0){
                    // New node
                    n->f = getHeuristic(n, h);
                    open_list.push(n);
                    open_set[getState(n->grid, n->charge)] = n;
                } else{
                    if(d < n->cost){
                        // Found a better path to exisiting neighbor
                        n->f = getHeuristic(n, h);
                        n->parent = cur;
                        open_list.push(n);
                    } else {

                    }
                }
            }
        }
    }
    cout << "No solution found." << endl;
}

int main(int argc, char** argv){
    int start1D;    
    if (argc < 2 || argc > 4 ){
        cout << "Usage: ./vacuum-plan-reference <alg> [ <heuristic> ]" << endl;
        cout << "where <alg> is either depth-first, depth-first-id, uniform-cost," << 
        " greedy, a-star, or ida-star, and if greedy, a-star or ida-star," <<
        " heuristic is either h0, h1, h2, h3 or battery heuristics h4, or h5." << endl;
        cout << "Expects a world on standard input." << endl;
        exit(0);
    } 

    string algo = argv[1];
    string heuristic = "";

    for (int i = 2; i < argc; ++i){
        string input = argv[i];
        if (input == "-battery"){
            BATTERY_FLAG = true;
        } else {
            heuristic = input;
        }
    } 

    string dim;
    getline (cin, dim);
    GRID_C = stoi(dim);
    getline (cin, dim);
    GRID_R = stoi(dim);
    MAX_CHARGE = GRID_C * 2 + GRID_R * 2 - 4;
    string grid = "";
    
    // Loop through individual cell from the input grid
    for (int i = 0; i < GRID_C * GRID_R; ++i){
        char out = getchar();
        if (out == '@'){
            start1D = i;
        } else if (out == '*'){
            ++DUST;
        }
        grid += out;
        
        if ( i % GRID_C == GRID_C - 1){
            getchar();
        }
    }
    
    string start_grid(grid);

    Node n = Node(start_grid, start1D % GRID_C, start1D / GRID_C);
    n.parent = NULL;
    n.cost = 0;
    n.vac = 0;
    n.id = ID++;
    n.charge = MAX_CHARGE;

    int h = -1;
    if (heuristic == "h0"){
        h = 0;
    } else if (heuristic == "h1"){
        h = 1;
    } else if (heuristic == "h2"){
        h = 2;
    } else if (heuristic == "h3"){
        h = 3;
    }

    if (algo == "d" || algo == "depth-first"){
        algoDFS(&n);
    } else if (algo == "u" || algo == "uniform-cost"){
        algoUniformCost(&n);
    } else if (algo == "id" || algo == "depth-first-id"){
        algoDFSid(&n);
    } else if (algo == "a" || algo == "a-star"){
        if (h != -1){
            algoAStar(&n, h);
        } else {
            cout << "Unknown heuristic: " << heuristic << endl;
            exit(0);
        }
    } else if (algo == "ida" || algo == "ida-star"){
        if (h != -1){
            algoAStarid(&n, h);
        } else {
            cout << "Unknown heuristic: " << heuristic << endl;
            exit(0);
        }
    } else if (algo == "g" || algo == "greedy"){
        if (h != -1){
            algoGreedy(&n, h);
        } else {
            cout << "Unknown heuristic: " << heuristic << endl;
            exit(0);
        }
    } else {
        cout << "Unknown algorithm: " << algo << endl;
        exit(0);
    }

    cout << N_GEN << " nodes generated" << endl;
    cout << N_EXP << " nodes expanded" << endl;
}

