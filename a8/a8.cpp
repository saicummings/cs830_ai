/*
 * CS830 Assignment 8
 * Sai Lekyang
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <queue>

using namespace std;

bool DEBUG = false;

int CONST_ID = 0;
int PRED_ID = 0;
int ACTION_ID = 0;
int N_EXP = 0;
int N_GEN = 0;
double WEIGHT = 0;
int ALGO = 0;

struct Node{
    Node* parent;
    int action; // action generated to get to this state
    uint64_t actionparam;
    double f;
    double g;
    double h;
    // [Bool[31] | Pred ID[30:]] [Param 0]...[Pram 7] 
    vector <bool> state;
};

struct Action{
    int id;
    int paramsize;
    // Each [] is 1 byte [Pred ID][Pram 0]..[Pram 6]
    vector <uint64_t> pre;
    vector <uint64_t> preneg;
    vector <uint64_t> del;
    vector <uint64_t> add;
};

unordered_map<string, unsigned int> P_TABLE;
unordered_map<string, unsigned int> C_TABLE;
unordered_map<string, unsigned int> A_TABLE;
unordered_map<int, string> PKEY_TABLE;
unordered_map<int, string> CKEY_TABLE;
unordered_map<int, string> AKEY_TABLE;
unordered_map<uint64_t, int> STATE_TABLE; // map state to index
unordered_map<int, uint64_t> SKEY_TABLE;
unordered_map<int, int> PARAM_TABLE; // predid map to number of param that pred contains

vector <int> GOAL; // use this to check the indicies of state to see if true
vector <int> GOALNEG; // use this to check the indicies of state to see if false
vector <Action> ACTIONS; // this is used for the branching factor
vector <uint64_t> PERMUTATIONS[16];

static void print_break(){
    cout << "================================" << endl;
}

string action_tostring(int action, uint64_t actionparam){
    string s = "";
    vector<int> p;
    while((actionparam & 0xFF) != 0){
        s = " " + CKEY_TABLE[actionparam & 0xFF] + s;
        actionparam = actionparam >> 8;
    }
    s = AKEY_TABLE[action] + s;
    return s;
}

void update_p(Node* n, uint64_t& key, bool b){
    int index = STATE_TABLE[key];
    n->state[index] = b;
}

void init_predicates_table(vector <string>& predicates){
    // blank spaces and "predicates:" already take care of
    for (string p : predicates){
        int params = 1;
        string predicate = "";
        for (char c : p){
            if (c == '('){
                P_TABLE[predicate] = ++PRED_ID;
                PKEY_TABLE[PRED_ID] = predicate;
            } else if ( c == ','){
                ++params;
            } else if (c == ')'){
                PARAM_TABLE[PRED_ID] = params;
            }
            predicate += c;
        }
    }

    if (DEBUG){
        for (auto const& x : P_TABLE){
            cout << x.first << ':' << x.second << endl ;
            cout << "  param size:" <<  PARAM_TABLE[ x.second ] << endl;
        }
        print_break();
    }
}

void init_constant_table(vector <string>& constants){
    for (string c : constants){
        C_TABLE[c] = ++CONST_ID;
        CKEY_TABLE[CONST_ID] = c;
    }

    if (DEBUG){
        for (auto const& x : C_TABLE){
            cout << x.first << ':' << x.second << endl ;
        }
        print_break();
    }
}

uint64_t get_actionkey(string& predicate, unordered_map<string, int>& param_table){
    uint64_t key = 0;
    int param = 0;
    string s = "";
    for(char c : predicate){
        if (c == '('){
            key = P_TABLE[s];
            key = key << 56; 
            s = "";
        } else if (c == ',' || c == ')'){
            uint64_t param_id = param_table[s];
            param_id = param_id << (52 - 4 * param++);
            key = key | param_id;
            s = "";
        } else if (c == ' '){
            // don't do jack
        } else {
            s += c;
        }
    }
    if(DEBUG){
        cout << "  " << predicate << endl;
        cout << "  " << hex << key << endl;
    }
    return key;
}

void init_actions(vector <vector <string>>& actions){
    for (vector<string> action : actions){
        Action a = Action();
        unordered_map<string, int> param_table;
        int param_id = 0;
        for (int i = 0; i < action.size(); ++i){
            stringstream ss(action[i]);
            string word;
            if (i == 0){
                ss >> word; // action name
                A_TABLE[word] = ++ACTION_ID;
                AKEY_TABLE[ACTION_ID] = word;
                a.id = ACTION_ID;
                int params = 0;
                string s;
                while(ss >> s){
                    param_table[s] = ++param_id;
                    ++params;
                }
                a.paramsize = params;
            } else {
                vector <uint64_t> v;
                ss >> word; // getting "pre:", "preneg:", "del:", etc

                if (DEBUG) cout << word << endl;

                string s;
                string predicate = "";
                while(ss >> s){
                    predicate += s;
                    // done with ont predicate, need its action key
                    if (predicate[predicate.size() - 1] == ')'){
                        v.push_back(get_actionkey(predicate, param_table));
                        predicate = "";
                    }
                }
                if (i == 1){
                    a.pre = v;
                } else if(i == 2){
                    a.preneg = v;
                } else if(i == 3){
                    a.del = v;
                } else if(i == 4){
                    a.add = v;
                }
            }
        }
        ACTIONS.push_back(a);
    }

    if (DEBUG) {
        print_break();
        for (Action a : ACTIONS){
            cout << "Name: " << AKEY_TABLE[a.id] << endl;
            cout << "ID: " << a.id << endl;
            cout << "Params: " << a.paramsize << endl;
            cout << "Pre:" << endl;
            for (uint64_t key : a.pre){
                cout << "  " << hex << key << endl;
            }
            cout << "Preneg:" << endl;
            for (uint64_t key : a.preneg){
                cout << "  " << hex << key << endl;
            }
            cout << "Del:" << endl;
            for (uint64_t key : a.del){
                cout << "  " << hex << key << endl;
            }
            cout << "Add:" << endl;
            for (uint64_t key : a.add){
                cout << "  " << hex << key << endl;
            }
            print_break();
        }
    }

}

uint64_t get_statekey(string& predicate){
    uint64_t key = 0;
    int param = 0;
    string s = "";
    for(char c : predicate){
        if (c == '('){
            key = P_TABLE[s];
            key = key << 56; 
            s = "";
        } else if (c == ',' || c == ')'){
            uint64_t param_id = C_TABLE[s];
            param_id = param_id << (48 - 8 * param++);
            key = key | param_id;
            s = "";
        } else if (c == ' '){
            // don't do jack
        } else {
            s += c;
        }
    }
    if(DEBUG){
        cout << "  " << predicate << endl;
        cout << "  " << hex << key << endl;
    }
    return key;
}

void ground_combinations(uint64_t key, unsigned int consts, int params, int depth){
    // recursive call to get the combinations
    uint64_t mask = consts;
    key = key | mask << (56 - 8 * depth);
    if (depth == params){
        int index =  STATE_TABLE.size();
        STATE_TABLE[key] = index;
        SKEY_TABLE[index] = key;

        if (DEBUG) cout << "  " << dec << index << ":" << hex << key << endl;

        return;
    }
    for (int c = 1; c <= CONST_ID; ++c){
        ground_combinations(key, c, params, depth + 1);
    }
}

void ground_state(){
    // generate groud state of uint64_t's 
    // using predicate and parameters
    // to determine the permutation of constants
    for (int i = 1; i <= PRED_ID; ++i){
        uint64_t key = i;
        key = key << 56;
        for (unsigned int c = 1; c <= CONST_ID; ++c){
            ground_combinations(key, c, PARAM_TABLE[i], 1);
        }
    }
}

void init_start(string& s, Node* n){
    if (DEBUG) cout << "Ground:" << endl;

    ground_state();

    if (DEBUG){
        print_break();
        cout << "Initial:" << endl;
    }    
    
    vector <bool> v(STATE_TABLE.size(), 0);
    n->state = v;
    n->action = 0;
    string predicate = "";
    bool flag = false;
    for (char c : s){
        if (c == ':') flag = true;
        else if (flag){
            if (c == ')'){
                predicate += c;
                uint64_t key = get_statekey(predicate);
                update_p(n, key, true);
                predicate = "";
            } else if (c == ' '){
                // don't do jack
            } else {
                predicate += c;
            }
        }
    }

    if (DEBUG){
        cout << endl;
        for (int i = 0; i < n->state.size(); ++i){
            if (n->state[i]){
                cout << "  " << dec << i << ":" << hex << SKEY_TABLE[i] << endl;
            }
        }
    }
}

void init_goal(string& s, bool sign){
    if (DEBUG && sign) cout << "Goal:" << endl;
    else if (DEBUG) cout << "Goalneg:" << endl;

    string cleangoal = "";
    bool flag = false;
    for (char c : s){
        if (c == ':') flag = true;
        else if (flag){
            if (c == ')'){
                cleangoal += c;
                uint64_t key = get_statekey(cleangoal);
                if (sign)
                    GOAL.push_back(STATE_TABLE[key]);
                else
                    GOALNEG.push_back(STATE_TABLE[key]);
                cleangoal = "";

                if (DEBUG) cout << "    index: " << dec << STATE_TABLE[key] << endl;

            } else if (c == ' '){
                // don't do jack
            } else {
                cleangoal += c;
            }
        }
    }
}

static int number(const string& s){
    string::const_iterator it = s.begin();
    while (it != s.end() && isdigit(*it)) ++it;
    if (!s.empty() && it == s.end()){
        return stoi(s);
    } else {
        return 0;
    }
}

void parse(Node* n){
    string line;
    vector <string> predicates;
    vector <string> constants;
    vector <vector <string>> actions;
    string inital;
    string goal;
    string goalneg;

    while(getline(cin, line)){
        if (line[0] == '#'){
            // skip comment line
        } else {
            stringstream ss(line);
            string word;
            ss >> word;
            if (word == "predicates:"){
                string predicate = "";
                bool flag = false;
                for (char c : line){
                    if (c == ':') flag = true;
                    else if (flag){
                        if (c == ')'){
                            predicate += c;
                            predicates.push_back(predicate);
                            predicate = "";
                        } else if (c == ' '){
                            // don't do jack
                        } else {
                            predicate += c;
                        }
                    }
                }
            } else if (word == "constants:"){
                string s;
                while(ss >> s){
                    constants.push_back(s);
                }
            } else if (word == "initial:"){
                inital = line;
            } else if (word == "goal:"){    
                goal = line;
            } else if (word == "goalneg:"){
                goalneg = line;
            } else {
                // check to see if it is the acton line 
                int acount = number(word);
                if (acount){
                    getline(cin, line); 
                    // given a number of actions 
                    for (int i = 0; i < acount ; ++i){
                        vector <string> action;
                        while (line.length() == 0 || line[0] == '#'){
                            getline(cin, line); 
                        } 
                        // pushes the action name and param into the vector
                        action.push_back(line);
                        for (int a = 0; a < 4; ++a){
                            getline(cin, line);
                            action.push_back(line);
                        }
                        if(i < acount - 1) getline(cin, line); 
                        actions.push_back(action);
                    }
                }
            }
        }
    }

    if (DEBUG){
        cout << "PREDICATES:" << endl;
        for (string s : predicates){
            cout << s << endl;
        }
        cout << endl;
        cout << "CONSTANTS:" << endl;
        for (string s : constants){
            cout << s << endl;
        }
        cout << endl;
        cout << "ACTIONS:" << endl;
        for (int i = 0; i < actions.size(); ++i){
            cout << i + 1 << endl;
            for (string a : actions[i]){
                cout << "  " << a << endl;
            }
        }
        cout << endl;
        cout << inital << endl;
        cout << endl;
        cout << goal << endl;
        cout << endl;
        cout << goalneg << endl;
        print_break();
    }

    init_predicates_table(predicates);
    init_constant_table(constants);
    init_actions(actions);
    init_start(inital, n);
    init_goal(goal, true);
    init_goal(goalneg, false);

    if (DEBUG){
        cout << "GOAL INDEX: ";
        for (int i : GOAL){
            cout << i << " ";
        }
        cout << "\nGOALNEG INDEX: ";
        for (int i : GOALNEG){
            cout << i << " ";            
        }
        cout << endl;
        print_break();
    }
}

void constant_combinations(uint64_t key, int paramsize, int consts, int depth){
    uint64_t mask = consts;
    // key = key | mask << (56 - 8 * depth);
    key = key << 8 | mask;
    if(depth == paramsize){
        PERMUTATIONS[paramsize - 1].push_back(key);
        // if (DEBUG) cout << "  " << hex << key << endl;
        return;
    }
    for (int c = 1; c <= CONST_ID; ++c){
        constant_combinations(key, paramsize, c, depth + 1);
    }
}


int get_stateindex(uint64_t action_predparam, uint64_t param, int action_paramsize){
    // return the index to the predicate in the state to check against
    uint64_t key = 0;
    int predid = action_predparam >> 56;
    key = predid;
    if (action_paramsize == 1){
        key = key << 8 | param;
        return STATE_TABLE[key << 48];
    }
    // will need to match the params to the right location
    vector<unsigned int> p;
    for (int i = action_paramsize; i > 0; --i){
        p.push_back(param >> (i * 8 - 8) & 0xFF);
    }
    int predparamsize = PARAM_TABLE[predid];
    for (int i = 0; i < predparamsize; ++i){
        int index = action_predparam >> ((13 - i) * 4) & 0xF;
        key = key << 8 | p[index - 1];
    }
    return STATE_TABLE[key << (56 - predparamsize * 8)];
}

bool is_goal(Node* n){
    for (int i = 0; i < GOAL.size(); ++i){
        if (n->state[GOAL[i]] == false)
            return false;
    }
    for (int i = 0; i < GOALNEG.size(); ++i){
        if (n->state[GOALNEG[i]] == true)
            return false;
    }
    vector<pair<int, uint64_t>> path;
    Node* t = n;
    while(t->parent != NULL){
        path.push_back(make_pair(t->action, t->actionparam));
        t = t->parent;
    }
    for (int i = path.size(); i > 0; --i){
        cout << path.size() - i << " "
        << action_tostring(path[i - 1].first, path[i - 1].second)
        << endl;
    }

    return true;
}

void get_children(Node* n, vector <Node*>& children){
    if (DEBUG) cout << "Children:" << endl;

    for (Action& a : ACTIONS){

        if (DEBUG) cout << AKEY_TABLE[a.id] << endl;

        // need permutations for the params
        if (PERMUTATIONS[a.paramsize - 1].size() == 0){
            uint64_t key = 0;
            for (unsigned int c = 1; c <= CONST_ID; ++c){
                constant_combinations(key, a.paramsize, c, 1);
            }
        }
        for (uint64_t param : PERMUTATIONS[a.paramsize - 1]){
            bool pre_flag = true;
            for (uint64_t k : a.pre){
                // check each pre
                int index = get_stateindex(k, param, a.paramsize);
                if (n->state[index] == false){
                    pre_flag = false;
                    break;
                }
            }
            if (!pre_flag) continue;
            bool preneg_flag = true;
            for (uint64_t k : a.preneg){
                // check each pre
                int index = get_stateindex(k, param, a.paramsize);
                if (n->state[index]){
                    preneg_flag = false;
                    break;
                }
            }
            if (!preneg_flag) continue;
            // passes all the pre and preneg at this point
            // need to set the adds and deletes
            Node* c = new Node;
            c->action = a.id;
            c->actionparam = param;
            c->state = n->state;
            c->parent = n;
            for (uint64_t k : a.add){
                // check each pre
                int index = get_stateindex(k, param, a.paramsize);
                c->state[index] = true;
            }
            for (uint64_t k : a.del){
                // check each pre
                int index = get_stateindex(k, param, a.paramsize);
                c->state[index] = false;
            }
            children.push_back(c);

            if (DEBUG){
                cout << "  " << action_tostring(a.id, param) << endl;
            }
        }

        if (DEBUG) print_break();
    }
}

int get_h(Node* n){
    if (ALGO == 0)
        return 0;
    int unsat = 0;
    int h = 0;
    int t = 0;
    int hmax = 0;
    for (int i : GOAL){
        if (!n->state[i]) ++unsat;
    }
    for (int i : GOALNEG){
        if (n->state[i]) ++unsat;
    }
    if (ALGO == 1) 
        return unsat * WEIGHT;
    vector <Node*> v;
    v.push_back(n);
    h = unsat;
    hmax = h;
    unordered_set <int> goal;
    unordered_set <int> goalneg;
    // need to find when literal became true    
    if (n->parent == NULL){
        for (int i = 0; i < n->state.size(); ++i){
            if (n->state[i]) goal.insert(i);
            else goalneg.insert(i);
        }
    } else {
        for (int i = 0; i < n->state.size(); ++i){
            if (n->state[i] != n->parent->state[i]){
                if (n->state[i]) goal.insert(i);
                else goalneg.insert(i);
            }
        }
    }
    while(1){
        vector <Node*> temp;
        for (Node* child : v){
            get_children(child, temp);
        }
        v.clear();
        for (Node* child : temp){
            v.push_back(child);
            // need to check which literals became true
            for (int i = 0; i < n->state.size(); ++i){
                if (child->parent->state[i]){
                    if (!child->state[i])
                    // prev was false, now true
                    goal.insert(i);
                }
                if (!child->parent->state[i]){
                    if (child->state[i])
                    // prev was true, now false
                    goalneg.insert(i);
                }
            }
        }
        ++t;
        // check to see if goals are true
        bool goal_flag = true;
        for (int i : GOAL){
            if (goal.find(i) == goal.end()){
                ++h;
            }
        }
        for (int i : GOALNEG){
            if (goalneg.find(i) == goalneg.end()){
                ++h;
            }
        }
        break; 
    }

    if (ALGO == 2){
        return WEIGHT * hmax;
    }

    return WEIGHT * h;
}

int get_g(Node* n){
    return 1;
}

void update_child_cost(Node* p, Node* c){
    c->g = p->g + 1;
    c->h = get_h(c);
    c->f = c->h + c->g;
}

class compare_f{
public:
    bool operator()(Node* a, Node* b) {
        return a->f > b->f;
    }
};

void astar(Node* root){
    priority_queue<Node*, vector<Node*>, compare_f> open_list;
    unordered_map<vector <bool>, Node*> open_set;
    unordered_map<vector <bool>, Node*> closed_list;
    open_list.push(root);
    open_set[root->state] = root;  
    while(!open_list.empty()){
        Node* cur = open_list.top();
        ++N_EXP;
        if (is_goal(cur)){
            return;
        }
        open_list.pop();
        open_set.erase(cur->state);
        closed_list[cur->state] = cur; 
        vector <Node*> children;
        get_children(cur, children);
        N_GEN += children.size();
        for (Node* c : children){
            // check for duplicate
            if (closed_list.find(c->state) != closed_list.end()){
                delete c;
            } else {
                update_child_cost(cur, c);
                if (open_set.find(c->state) == open_set.end()){
                    // new node, not in open list
                    open_list.push(c);
                    open_set[c->state] = c;
                } else {
                    // check to see if this new node is better one in open
                    Node* n = open_set[c->state];
                    if (c->g < n->g){
                        n->f = c->f;
                        n->g = c->g;
                        n->h = c->h;
                        n->parent = c->parent;
                    }
                    delete c;
                }
            }
        }
    }
    cout << "No plan found." << endl;
}

int main(int argc, char** argv){
    if (argc < 3 ){
        cout << "Usage: ./a <weight> <heuristic>" << endl;
        cout << "Where <weight> is the weight for weighted A* and" << endl;
        cout << "and <heuristic> is either h0, h-goal-lits, h1, or h1sum." << endl;
        cout << "Expects a problem domain and instance on standard input." << endl;
        cout << "Expects a world on standard input." << endl;
        exit(0);
    } 

    WEIGHT = stod(argv[1]);

    string algo = argv[2];

    if (algo == "h0"){
        WEIGHT = 1;
    } else if (algo == "h-goal-lits"){
        ALGO = 1; 
    } else if (algo == "h1"){
        ALGO = 2; 
    } else if (algo == "h1sum"){
        ALGO = 3; 
    } else {
        cout << "Unknow algorithm: " << algo << endl;
        exit(0);
    }

    for (int i = 0; i < argc; ++i){
        string input = argv[i];
        if (input == "-debug" ){
            DEBUG = true;
        } 
    }

    Node* start = new Node();
    start->parent = NULL;
    start->g = 0;
    start->f = get_h(start);
    parse(start);
    astar(start);
    cout << N_GEN << " nodes generated" << endl;
    cout << N_EXP << " nodes expanded" << endl;
}
