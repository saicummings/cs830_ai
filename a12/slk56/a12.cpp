#include <ostream>
#include <sstream>
#include <vector>
#include <iostream>
#include <random>

using namespace std;
struct tup{
    int r;
    int c;
    double p;
    tup(int r, int c) : r(r), c(c){}
};

double sigma = 2;
double particles = 100;
int map_h;
int map_w;
vector <string> grid;
vector<tup> pos_p;

vector<tup> make_particles(int size){
    vector<tup> vec;
    while(vec.size() < size){
        int row = rand() % map_h;
        int col = rand() % map_w;
        if (grid[row][col] != '#'){
            bool dup = false;
            vec.push_back(tup(row, col));
        }
    }
    return vec;
}

void mcl(double x, double y, string dir){
    /*
    S ← samples from prior
    w ← uniform distribution
    */
    int i = 0;
    for (auto p : pos_p){
        tup loc = p;
        int prob = rand() % 10;
        // robot execute action 0.7
        // robot rotate 90 left or right 0.1
        // robot does not move 0.1

        if(prob != 2) {
            
            if (dir == "NORTH" || dir == "SOUTH"){
                if (prob > 2){
                    loc.r += (dir == "NORTH") ? 1 : -1;
                }
                else {
                    loc.c += (prob) ? 1: -1;
                }
            } else {
                if (prob > 2){
                    loc.c += (dir == "EAST") ? 1: -1;   
                }
                else {
                    loc.r += (prob) ? 1: -1;
                }
            } 
        }

        if (loc.r >= map_h || loc.r < 0 || loc.c < 0 || loc.c >= map_w){
            // do nothing
        } else if (grid[loc.r][loc.c] == '#'){
            // do nothing            
        } else {
            pos_p[i].c = loc.c;
            pos_p[i].r = loc.r;
        }
        double ex = -((pow((pos_p[i].c - x), 2.0) / (2.0*pow(sigma,2.0))) + 
            (pow((pos_p[i].r - y), 2) / (2.0*pow(sigma, 2))));
        pos_p[i].p = exp(ex);
	    cout << pos_p[i].c << " " << pos_p[i].r << " " << pos_p[i].p << endl;
        ++i;
    }
    
    // rejuvenate 10%
    vector <tup> rejuv = make_particles(particles/10);

    // S ← sample from S with P(si) ∝ wi
    double range = 0;
    for (tup t : pos_p){
        range += t.p;
    }
    default_random_engine generator;
    uniform_real_distribution<double> distribution(0.0, range);
    while (rejuv.size() != particles){
        double sample = distribution(generator);
        double sum = 0;
        i = 0;
        for (tup t : pos_p){
            if (sum + t.p >= sample){
                rejuv.push_back(pos_p[i]);
                break;
            }
            sum += t.p;
            ++i;
        }
    }
    pos_p = rejuv;
}

int main(int argc, char** argv) {
    vector <string> temp_grid;
    string line;
    int r = 0;
  	default_random_engine generator(time(NULL));
    srand (time(NULL));
    for(int i = 0; i< argc; i++){
	    string a = argv[i];
	    if(a == "--sigma"){
	        sigma = stoi(argv[i + 1]);
	    }
        if(a == "--particles"){
	        particles = stoi(argv[i + 1]);
	    }
	}
    while(getline(cin, line)){
        string word;
        stringstream ss(line);
        if (line == "END"){
            break;
        } else if (r == 0){
            ss >> map_w;
        } else if (r == 1){
            ss >> map_h;
        } else if (r < map_h + 2){
            temp_grid.push_back(line);
        } else {
            if (r == map_h + 2){
                for (int i = temp_grid.size() - 1; i >= 0; --i){
                    // (0,0) in bottom left, need to reverse
                    grid.push_back(temp_grid[i]);
                }
                pos_p = make_particles(particles);
            }
            double ux;
            double uy;
            string dir;
            ss >> ux;
            ss >> uy;
            ss >> dir;
            mcl(ux, uy, dir);
        }
        ++r;
    }
}
