/*
 * CS830 Assignment 7
 * Sai Lekyang
 */

#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>
#include <set>
#include <functional>
#include <math.h> 
#include <algorithm>
#include <vector>

using namespace std;

char DEBUG = false;
int cnfs_cnt = 1;
int RES = 0;
vector<string> outlist;

struct Node{
    int type; // 0 = constant, 1 = function, 2 = variable
    int sign = -1;
    string name;
    vector<Node*> children; // Only for functions
};

void printtreehelper(Node* n, unordered_map<string, Node*>& unify){
    cout << n->name;
    if (n->type == 1){
        cout << "(";
    }
    int i = 0;
    for( Node* c : n->children){
        if (i > 0)
            cout << ",";

        if (c->type == 2)
            printtreehelper(unify[c->name], unify);
        else
            printtreehelper(c, unify);

        ++i;
    }
    if (n->type == 1){
        cout << ")";
    }
}

void printtree(vector<Node*> clauses, unordered_map<string, Node*>& unify){
    int v = 0;
    for (Node* c : clauses){
        if (v++ > 0)
            cout << " | ";
        if (!c->sign)
            cout << "-";
        cout << c->name << "(";
        int i = 0;

        for( Node* n : c->children){
            if (i > 0)
                cout << ",";
            if (n->type == 2)
                printtreehelper(unify[n->name], unify);
            else
                printtreehelper(n, unify);
            ++i;
        }
        cout << ")";
    }
}

void printclause(Node* n){
    cout << n->name;
    if (n->type == 1){
        cout << "(";
    }
    
    int i = 0;
    for( Node* c : n->children){
        if (i > 0)
            cout << ",";
        printclause(c);
        ++i;
    }
    if (n->type == 1){
        cout << ")";
    }
}

void printsentence(vector<Node*> clauses){
    int v = 0;
    for (Node* c : clauses){
        if (v++ > 0)
            cout << " | ";
        if (!c->sign)
            cout << "-";
        cout << c->name << "(";
        int i = 0;

        for( Node* n : c->children){
            if (i > 0)
                cout << ",";
            printclause(n);
            ++i;
        }
        cout << ")";
    }
}

// takes a clause and makes a tree
Node* maketree(string s){
    string name = "";

    // Variable
    if (s[0] == 'v'){
        Node* n = new Node();
        n->name = s;
        n->type = 2;
        return n;
    } 

    // Function
    for (int i = 0; i < s.length(); ++i){
        if (s[i] == '('){
            Node* n = new Node();
            n->name = name;
            n->type = 1;
            vector<string> children;

            name = "";

            int brac = 0;
            bool func = false;
            // Need to delimiate on , but because of inner fuctions with ,
            // which will need to skip over
            for (i = i + 1; i < s.length(); ++i){
                char cur = s[i];   
                
                if(cur == '('){
                    func = true;
                    ++brac;
                } else if(cur == ')'){
                    --brac;
                }

                if (brac == 0 && func){
                    func = false;
                }

                if ((!func && cur == ',') || brac < 0 || (func && brac == 0)){
                    children.push_back(name);
                    name = "";
                    brac = 0;
                    func = false;
                    ++i;
                } 
                name += s[i];
            }
            
            for (string c : children){
                Node * child = maketree(c);
                (n->children).push_back(child);
            }
            
            return n;
        }
        name += s[i];
    }


    // Constant
    Node* n = new Node();
    n->name = s;
    n->type = 0;

    return n;
}

// takes an vector of clauses string and turn it into vector of clause tree nodes
// and add it to the list
void maketree(vector<string>& clauses_str, vector< vector<Node*> >& cnfs){
    vector<Node*> cnf;
    for (string s : clauses_str){
        char sign = 1;
        int i = 0;
        Node* c = new Node();

        if (s[0] == '-'){
            sign = 0;
            ++i;
        }

        string str = "";

        while(s[i] != '('){
            str += s[i];
            ++i;
        }

        c->name = str;
        c->type = 1; 
        c->sign = sign;

        str = "";
        int open_brack = 0;
        int close_brack = 0;

        // literals within the clause
        for(++i; i < s.length(); ++i){
            if (s[i] == '('){
                ++open_brack;
            } else if (s[i] == ')'){
                ++close_brack;
            }  
            
            if ((s[i] == ',' && open_brack - close_brack <= 0 )|| open_brack - close_brack < 0){
                c->children.push_back(maketree(str));
                str = "";
            } else if (s[i] != ' '){
                str += s[i];
            }
        }
        cnf.push_back(c);
    }
    //printsentence(cnf);
    cnfs.push_back(cnf);
}

// splits a sentance into a vector of clauses
// need to take those clauses and make an array of nodes
void seprateclauses(string line, int& counter, vector< vector<Node*> >& cnfs){
    unordered_map <string, string> dict;
    vector <string> clauses;
    string s = "";
    bool clause = true;
    bool funconst = false;
    for(int i = 0; i <= line.length(); ++i){
        char cur = line[i];
        
        if (clause && cur == '('){
            clause = false;
        } 

        if (!clause && (cur >= 'A' and cur <= 'Z')){
            funconst = true;
        }

        if (funconst && ( cur == '(' || cur == ',')){
            funconst = false;
        } 
        
        if (cur == '|' || i == line.length()){
            clauses.push_back(s);
            s = "";
            clause = true;
            funconst = false;
        } else if (!clause && !funconst && (cur >= 'a' and cur <= 'z')){
            string v = "";
            while( cur != ' ' && cur != ',' && cur != ')'){
                v += cur;
                ++i;
                cur = line[i];
            }
            if (dict.find(v) != dict.end()){
                v = dict[v];
            } else {
                dict[v] = "v" + to_string(counter);
                v = "v" + to_string(counter);
                ++counter;
            }
            s += v;
            --i;
        } else if (cur != ' ') {
            s += cur;
        }
    }

    maketree(clauses,cnfs);
}

bool checkchildren(Node* v, Node* func, unordered_map<string, Node*>& unify){
    int r;
    for (Node *c : func->children){
        if (c->type == 2){
            c = unify[c->name];
        }
        if (c->type == 1){ 
            if( checkchildren(v, c, unify) == 0)
                return false;
        } else if (v->name == c->name){
            return false;
        }
    }
    return true;
}

bool unifyhelper(Node* a, Node* b, unordered_map<string, Node*>& unify){
    // If a is a constant
    
    if(a->type == 0){
        if (DEBUG){
            cout << "    case 0" << endl;
        }
        // If b is a constant and same name
        if (b->type == 0 && a->name == b->name){
            return true;
        } 
        // If b is a function, fail

        // If b is a var, b is now pointing to a
        else if (b->type == 2){
            // unify[b->name] = a;
            for (auto it : unify){
                if(DEBUG) cout << "    ." << it.first << "->" << it.second->name << endl;
                if(it.second == b){
                    unify[it.first] = a;
                }
            }
            return true;
        }
    } 
    // If a is a function
    else if (a->type == 1){
        if (DEBUG){
            cout << "    case 1: a is a function" << endl;

        }
        // If b is a constant, fail

        // If b is a funtion, unify the arguments
        if (b->type == 1){
            if (DEBUG){
                cout << "      b is a funtion" << endl;
            }
            if (a->children.size() != a->children.size())
                return false;
            int r;
            for (int c = 0; c < a->children.size(); ++c){
                Node* aa = a->children[c];
                Node* bb = b->children[c];

                if (aa->type == 2)
                    aa = unify[aa->name];
                if (bb->type == 2)
                    bb = unify[bb->name];
                
                if (DEBUG){
                    cout << "  " << aa->name << " : " << bb->name << endl;
                }

                if (unifyhelper(aa, bb, unify) == false)
                    return false;

            }
            return 1;
        }
        // If b is a variable
        if (b->type == 2){
            if (DEBUG){
                cout << "      b is a variable" << endl;
            }
            // Check to see if b is in a
            if (checkchildren(b, a, unify) == false)
                return false;
            for (auto it : unify){
                if(DEBUG) cout << "    ." << it.first << "->" << it.second->name << endl;
                if(it.second == b){
                    unify[it.first] = a;
                }
            }
            return true;
        }
    // If a is variable
    } else {
        if (DEBUG){
            cout << "    case 2" << endl;
        }

        // If b is a constant
        if (b->type == false){
            if (DEBUG){
                cout << "      b is a variable" << endl;
            }
            for (auto it : unify){
                if(DEBUG) cout << "    ." << it.first << "->" << it.second->name << endl;                
                if(it.second == a){
                    unify[it.first] = b;
                }
            }
            return true;
        } 
        // If b is function
        else if (b->type == 1){
            if (DEBUG){
                cout << "      b is a funtion" << endl;
            }

            // Check to see if b is in a
            if (checkchildren(a, b, unify) == false)
                return false;

            for (auto it : unify){
                if(DEBUG) cout << "    ." << it.first << "->" << it.second->name << endl;

                if(it.second == a){
                    unify[it.first] = b;
                }
            }
            return true;
        } else {
            // Else sub the vars
            for (auto it : unify){
                if(DEBUG) cout << "    ." << it.first << "->" << it.second->name << endl;

                if(it.second == a){
                    unify[it.first] = b;
                }
            } 
            return true;
        }
    }
    return false;
}

bool unifyClause(Node* a, Node* b, unordered_map<string, Node*>& unify){
    /*
    if (DEBUG){
        cout << "[DEBUG] unifying ";
        if (a->sign == 0)
            cout << "-";
        printclause(a);
        cout << " with ";
        if (b->sign == 0)
            cout << "-";
        printclause(b);
        cout << endl;
    }
    */

    for(int i = 0; i < a->children.size(); ++i){
        Node* aa = a->children[i];
        Node* bb = b->children[i];
        if (aa->type == 2)
            aa = unify[aa->name];
        if (bb->type == 2)
            bb = unify[bb->name];

        if (DEBUG){
            cout << "  ";
            printclause(aa);
            cout << " :: ";
            printclause(bb);
            cout << endl;
        }

        bool r = unifyhelper(aa, bb, unify);

        if (r == false){
            return false;
        }

    }
    return true;
}

void inithashhelper(Node* n, unordered_map<string, Node*>& unify){
    if (n->type == 2){
        unify[n->name] = n;
    }
    for( Node* c : n->children){
        inithashhelper(c, unify);
    }
}

// go through the two sentences, look for variables and add it to the hash
void inithash(vector<Node*> a,  vector<Node*> b, unordered_map<string, Node*>& unify){

    for(Node* c : a){
        for( Node* n : c->children){
            inithashhelper(n, unify);
        }
    }


    for(Node* c : b){
        for( Node* n : c->children){
            inithashhelper(n, unify);
        }
    }
}

void newsupporthelper(Node* n, unordered_map<string, Node*>& unify, string& newsupport){
    newsupport += n->name;
    if (n->type == 1){
        newsupport += "(";
    }
    int i = 0;
    for( Node* c : n->children){
        if (i > 0)
            newsupport += ",";

        if (c->type == 2)
            newsupporthelper(unify[c->name], unify, newsupport);
        else
            newsupporthelper(c, unify, newsupport);

        ++i;
    }
    if (n->type == 1){
        newsupport += ")";
    }
}

// make a new sentence without a[i], and a[j]
bool newsupportstr(vector<Node*> a, vector<Node*> b, int n, int m, 
   unordered_map<string, Node*>& unify, vector< vector<Node*> >& support, int& counter, int s, int k){
    string newsupport = "";
    int v = 0;
    int as = 0;
    int bs = 0;
    bool aans = false;
    bool bans = false;


    if (DEBUG){
        cout << "new support without " << n << " and " << m << endl; 
        printsentence(a);
        cout << endl;
        printsentence(b);
        cout << endl;
    }
    
    for (int i = 0; i < a.size(); ++i){
        Node* c = a[i];
        if (i != n){
            if (v++ > 0)
                newsupport += " | ";
            if (!c->sign)
                newsupport += "-";
            newsupport += c->name + "(";
            int i = 0;

            for( Node* n : c->children){
                if (i > 0)
                    newsupport += ",";
                if (n->type == 2)
                    newsupporthelper(unify[n->name], unify, newsupport);
                else
                    newsupporthelper(n, unify, newsupport);
                ++i;
            }
            if (i > 0)
                newsupport += ")";

            ++as;
            if( c->name == "Ans")
                aans = true;
        }
    }
    
    for (int i = 0; i < b.size(); ++i){
        Node* c = b[i];
        if (i != m){
            if (v++ > 0)
                newsupport += " | ";
            if (!c->sign)
                newsupport +=  "-";
            newsupport += c->name + "(";
            int i = 0;
            for( Node* n : c->children){
                if (i > 0)
                    newsupport += ",";
                if (n->type == 2)
                    newsupporthelper(unify[n->name], unify, newsupport);
                else
                    newsupporthelper(n, unify, newsupport);
                ++i;
            }
            if (i > 0)
                newsupport += ")";

            ++bs;            
            if( c->name == "Ans")
                bans = true;
        }
    }

    if (DEBUG)
        cout << "[DEBUG] new support: " << newsupport << endl;
    

    ++RES;

    if (newsupport == ""){
        outlist.push_back( to_string(k) + " and " + to_string(s) + " give " + to_string(cnfs_cnt) + ": <empty>");
        // cout << k << " and " << s << " give " << cnfs_cnt << ": <empty>" << endl;
        return true;
    }

    outlist.push_back( to_string(k) + " and " + to_string(s) + " give " + to_string(cnfs_cnt) + ": " + newsupport);
    // cout << k << " and " << s << " give " << cnfs_cnt << ": " << newsupport << endl;

    if (as == 1 && bs == 0 && aans){
        return true;
    }

    if (bs == 1 && as == 0 && bans){
        return true;
    }

    ++cnfs_cnt;
    seprateclauses(newsupport, counter, support);
    return false;
}

// resolution on two sentences
bool resolution(vector<Node*> a,  vector<Node*> b, vector< vector<Node*> >& support, int& counter, int s, int k){
    /*
    if (DEBUG){
        cout << "[DEBUG] resolution on ";
        printsentence(a);
        cout << " :: ";
        printsentence(b);
        cout << endl;
    }
    */

    unordered_map<string, Node*> unify;
    inithash(a, b, unify);
 
    vector<Node*> new_clause;
    for (int i = 0; i < a.size(); ++i){
        for(int j = 0; j < b.size(); ++j){
            if (a[i]->name != b[j]->name || a[i]->sign == b[j]->sign){
                continue;
            } 

            if (a[i]->children.size() != b[j]->children.size()){
                continue;
            }

            bool r;
            r = unifyClause(a[i], b[j], unify);

            if (DEBUG){
                if (r)
                    cout << "[DEBUG] :) valid" << endl;
                else
                    cout << "[DEBUG] :( invalid" << endl;
            }       

            //Node* c = NULL;
            //resolutionhelper(a[i], b[j], new_clause);
            if (r){
                if (newsupportstr(a, b, i, j, unify, support, counter, s, k))
                    return true;
            }
        }
    }
    return false;
}

int main(int argc, char** argv){
    int counter = 0;

    string line;
    vector<string> sentences;
    vector<string> query;
    vector< vector<Node*> > cnfs; // sentences of cnfs
    vector< vector<Node*> > support; // sentences of query and support

    for (int i = 0; i < argc; ++i){
        string input = argv[i];
        if (input == "-debug" ){
            DEBUG = true;
        } 
    }

    // line = line.trim();
    getline(cin, line);    
    while(line != "--- negated query ---"){
        sentences.push_back(line);
        seprateclauses(line, counter, cnfs);
        cout << cnfs_cnt << ": "; 
        printsentence(cnfs[cnfs_cnt -  1]);
        cout << endl;
        ++cnfs_cnt;
        getline(cin, line);
    }
    if (DEBUG)
        cout << "[DEBUG] --- negated query ---" << endl;

    while(getline(cin, line)){
        query.push_back(line);
        seprateclauses(line, counter, support);
        cout << cnfs_cnt << ": "; 
        printsentence(support[cnfs_cnt - cnfs.size() -  1]);
        cout << endl;
        ++cnfs_cnt;
    }

    int largest_start_cnf = 0;

    // making vectors to sort from smallest to largest clauses
    vector<pair<int,int>> cnf_indx_size;
    vector<pair<int,int>> support_indx_size;
    for(int i = 0; i < cnfs.size(); ++i){
        if (cnfs[i].size() > largest_start_cnf){
            largest_start_cnf = cnfs[i].size();
        }
        cnf_indx_size.push_back(make_pair(cnfs[i].size(), i));
    }
    for(int i = 0; i < support.size(); ++i){
        if (support[i].size() > largest_start_cnf){
            largest_start_cnf = support[i].size();
        }
        support_indx_size.push_back(make_pair(support[i].size(), i));
    }

    sort(cnf_indx_size.begin(), cnf_indx_size.end());
    sort(support_indx_size.begin(), support_indx_size.end());

    // vectors are now sorted from smallest to largest clauses
    int cnf_indx = 0;
    int support_indx = 0;
    int supportsize = 0;
    bool empty = false;
    bool unsolvable = false;
    set <unsigned int> set1;
    set <unsigned int> set2;
    unsigned int a_indx;
    unsigned int b_indx;
    unsigned int key;

    while(supportsize != support.size()){
        supportsize = support.size();
        for (int i = 0; i < supportsize; ++i){
            a_indx = support_indx_size[i].second;
            vector<Node*> a = support[a_indx];

            for (int j = 0; j < cnfs.size(); ++j){
                b_indx = cnf_indx_size[j].second;
                vector<Node*> b = cnfs[b_indx];

                key = a_indx << 8 | b_indx;
                if (set1.find(key) == set1.end()){
                    // these are the numbers for output e.g. sn and kn gives #
                    int sn = a_indx + cnfs.size() + 1;
                    int kn = b_indx + 1;
                    empty = resolution(a, b, support, counter, sn, kn);
                    if (empty)
                        break;
                    set1.insert(key);
                }

                if (supportsize != support.size()){
                    int before = supportsize;
                    int after = support.size();

                    for (int l = 0; l < support.size() - supportsize; ++l){
                        int ssize = support.size();
                        support_indx_size.push_back(make_pair(support[ssize - 1 - l].size(), ssize - 1 - l));
                        if (support[ssize - 1 - l].size() > largest_start_cnf * 4){
                            unsolvable = true;
                        }
                    }

                    sort(support_indx_size.begin(), support_indx_size.end());
                    break;
                }
            }
            if (empty || supportsize != support.size() || unsolvable)
                break;

            // tried all the kb, now comparing support to support
            for (int j = 0; j < supportsize; ++j){
                b_indx = support_indx_size[j].second;

                if (a_indx == b_indx)
                    continue;

                vector<Node*> b = support[b_indx];
                key = a_indx << 8 | b_indx;
                if (set2.find(key) == set2.end()){
                    // these are the numbers for output e.g. sn and kn gives #
                    int sn = a_indx + cnfs.size() + 1;
                    int kn = b_indx + 1;
                    empty = resolution(a, b, support, counter, sn, kn);
                    if (empty)
                        break;
                    set2.insert(key);
                }

                if (supportsize != support.size()){
                    int before = supportsize;
                    int after = support.size();

                    for (int l = 0; l < support.size() - supportsize; ++l){
                        int ssize = support.size();
                        support_indx_size.push_back(make_pair(support[ssize - 1 - l].size(), ssize - 1 - l));
                        if (support[ssize - 1 - l].size() > largest_start_cnf * 4){
                            unsolvable = true;
                        }
                    }

                    sort(support_indx_size.begin(), support_indx_size.end());
                    break;
                }
            }
            if (empty || supportsize != support.size() || unsolvable)
                break;
        }

        if(empty || unsolvable)
            break;
    }

    if (!empty){
        cout << "No proof exists." << endl;
    } else {
        for (string s : outlist){
            cout << s << endl;
        }
    }
    cout << RES << " total resolutions" << endl;
}
