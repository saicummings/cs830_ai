/*
 * CS830 Assignment 6
 * Sai Lekyang
 */

#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>
#include <functional>
#include <math.h> 
#include <algorithm>
#include <vector>

using namespace std;

char DEBUG = 0;
struct Node{
    int type; // 0 = constant, 1 = function, 2 = variable
    string name;
    Node* parent;
    vector<Node*> children; // Only for functions
};

struct Clause{
    int sign;
    string name;
    vector<string> literals;
    vector<Node*> children;
};

Node * treehelper(string s, Node* parent, unordered_map<string, Node*>& unify, int depth){
    if (DEBUG < 0){
        for (int d = 0; d < depth; ++d)
            cout << " ";
        cout << s << endl;
    }
    string name = "";
    // Variable
    if (s[0] == 'v'){
        if (unify.find(s) != unify.end()){
            return unify[s];
        } else {
            Node* n = new Node();
            n->name = s;
            n->type = 2;
            unify[s] = n;
            return n;
        }
    } 

    // Function
    for (int i = 0; i < s.length(); ++i){
        if (s[i] == '('){
            Node* n = new Node();
            n->name = name;
            n->parent = parent;
            n->type = 1;
            vector<string> children;

            name = "";

            int brac = 0;
            bool func = false;
            // Need to delimiate on , but because of inner fuctions with ,
            // which will need to skip over
            for (i = i + 1; i < s.length(); ++i){
                char cur = s[i];   
                
                if(cur == '('){
                    func = true;
                    ++brac;
                } else if(cur == ')'){
                    --brac;
                }

                if (brac == 0 && func){
                    func = false;
                }

                if ((!func && cur == ',') || brac < 0 || (func && brac == 0)){
                    children.push_back(name);
                    name = "";
                    brac = 0;
                    func = false;
                    ++i;
                } 
                name += s[i];
            }
            
            for (string c : children){
                Node * child = treehelper(c, n, unify, depth + 1);
                (n->children).push_back(child);
            }
            
            return n;
        }
        name += s[i];
    }

    // Constant
    Node* n = new Node();
    n->name = s;
    n->type = 0;
    n->parent = parent;

    return n;
}

void printtreehelper(Node* n, unordered_map<string, Node*>& unify){
    cout << n->name;
    if (n->type == 1){
        cout << "(";
    }
    int i = 0;
    for( Node* c : n->children){
        if (i > 0)
            cout << ",";

        if (c->type == 2)
            printtreehelper(unify[c->name], unify);
        else
            printtreehelper(c, unify);

        ++i;
    }
    if (n->type == 1){
        cout << ")";
    }
}

void printtree(vector<Clause>& clauses, unordered_map<string, Node*>& unify){
    int v = 0;
    for (Clause c : clauses){
        if (v++ > 0)
            cout << " | ";
        if (!c.sign)
            cout << "-";
        cout << c.name << "(";
        int i = 0;

        for( Node* n : c.children){
            if (i > 0)
                cout << ",";
            if (n->type == 2)
                printtreehelper(unify[n->name], unify);
            else
                printtreehelper(n, unify);
            ++i;
        }
        cout << ")";
    }
}

int checkchildren(Node* v, Node* func, unordered_map<string, Node*>& unify){
    if (DEBUG)
        cout << "    check children " << v->name << " - " << func->name << endl;
    int r;

    for (Node *c : func->children){
        if (c->type == 2){
            c = unify[c->name];
        }

        if (DEBUG)
            cout << "      " << func->name << " kid: " << c->name << " " << c->type <<  endl;
        if (c->type == 1){ 
            if( checkchildren(v, c, unify) == 0)
                return 0;

        } else if (v->name == c->name){
            return 0;

        }

    }
  
    return 1;
}

int unifyhelper(Node* a, Node* b, unordered_map<string, Node*>& unify){
    // if (a->type == 2){
    //     a = unify[a->name];
    // }

    // if (b->type == 2){
    //     b = unify[b->name];
    // }

    // If a is a constant
    if(a->type == 0){
        if (DEBUG){
            cout << "    case 0" << endl;
        }
        // If b is a constant and same name
        if (b->type == 0 && a->name == b->name){
            return 1;
        } 
        // If b is a function, fail

        // If b is a var, b is now pointing to a
        else if (b->type == 2){
            // unify[b->name] = a;
            for (auto it : unify){
                if(DEBUG) cout << "." << it.first << "->" << it.second->name << endl;
                if(it.second == b){
                    unify[it.first] = a;
                }
            }
            return 1;
        }
  
    } 
    // If a is a function
    else if (a->type == 1){
        if (DEBUG){
            cout << "    case 1: a is a function" << endl;

        }

        // If b is a constant, fail

        // If b is a funtion, unify the arguments
        if (b->type == 1){
            if (DEBUG){
                cout << "      b is a funtion" << endl;
            }
            if (a->children.size() != a->children.size())
                return 0;

            int r;
            for (int c = 0; c < a->children.size(); ++c){
                Node* aa = a->children[c];
                Node* bb = b->children[c];

                if (aa->type == 2)
                    aa = unify[aa->name];
                if (bb->type == 2)
                    bb = unify[bb->name];

                if (DEBUG){
                    cout << "  " << aa->name << " : " << bb->name << endl;
                }

                if (unifyhelper(aa, bb, unify) == 0)
                    return 0;

            }


            return 1;
        }
        // If b is a variable
        if (b->type == 2){
            if (DEBUG){
                cout << "      b is a variable" << endl;
            }
            // Check to see if b is in a
            if (checkchildren(b, a, unify) == 0)
                return 0;

            // unify[b->name] = a;
            for (auto it : unify){
                if(DEBUG) cout << "." << it.first << "->" << it.second->name << endl;
                if(it.second == b){
                    unify[it.first] = a;
                }
            }
            return 1;
        }
    // If a is variable
    } else {
        if (DEBUG){
            cout << "    case 2" << endl;

        }
        // If b is a constant
        if (b->type == 0){
            if (DEBUG){
                cout << "      b is a variable" << endl;
            }
            for (auto it : unify){
                if(DEBUG) cout << "." << it.first << "->" << it.second->name << endl;
                if(it.second == a){
                    unify[it.first] = b;
                }
            }
            //unify[a->name] = b;
            return 1;
        } 
        // If b is function
        else if (b->type == 1){
            if (DEBUG){
                cout << "      b is a funtion" << endl;
            }

            // Check to see if b is in a
            if (checkchildren(a, b, unify) == 0)
                return 0;

            for (auto it : unify){
                if(DEBUG) cout << "." << it.first << "->" << it.second->name << endl;
                if(it.second == a){
                    unify[it.first] = b;
                }
            }

            // unify[a->name] = b;
            return 1;
        } else {
 
            // Else sub the vars

            for (auto it : unify){
                if(DEBUG) cout << "." << it.first << "->" << it.second->name << endl;
                if(it.second == a){
                    unify[it.first] = b;
                }
            } 

            // unify[a->name]->name = b->name;

            return 1;
        }
    }
    return 0;
}

int unifyClause(Clause& a, Clause& b, unordered_map<string, Node*>& unify){
    if (a.name != b.name || a.sign == b.sign){
        return 0;
    } 

    if (a.children.size() != b.children.size()){
        return 0;
    }

    if (DEBUG){
        int d = 0;
        cout << "Unifying " << a.sign << a.name << "(";
        for (string s : a.literals){
            if (d++ > 0)
                cout << ",";
            cout << s;
        } 
        cout << ")" << " with " << b.sign << b.name << "(";
        d = 0;
        for (string s : b.literals){
            if (d++ > 0)
                cout << ",";
            cout << s;
        } 
        cout << ")" << endl;
    }

    for(int i = 0; i < a.children.size(); ++i){
        Node* aa = a.children[i];
        Node* bb = b.children[i];

        if (aa->type == 2)
            aa = unify[aa->name];
        if (bb->type == 2)
            bb = unify[bb->name];

        if (DEBUG){
            cout << "  " << aa->name << " : " << bb->name << endl;
        }

        int r = unifyhelper(aa, bb, unify);

        if (DEBUG){
            cout << endl;
            Clause c = a;
            if (!c.sign)
                cout << "-";
            cout << c.name << "(";
            int j = 0;

            for( Node* n : c.children){
                if (j > 0)
                    cout << ",";
                if (n->type == 2)
                    printtreehelper(unify[n->name], unify);
                else
                    printtreehelper(n, unify);
                ++j;
            }
            cout << ")";

            cout << endl;

            c = b;
            if (!c.sign)
                cout << "-";
            cout << c.name << "(";
            j = 0;

            for( Node* n : c.children){
                if (j > 0)
                    cout << ",";
                if (n->type == 2)
                    printtreehelper(unify[n->name], unify);
                else
                    printtreehelper(n, unify);
                ++j;
            }
            cout << ")" << endl;
            cout << endl;
            for (auto it : unify){
                if(DEBUG) cout << "." << it.first << "->" << it.second->name << endl;
            }
            cout << endl;

        }
        if (r == 0){
            return 0;
        }

    }

    return 1;
}

void buildtree(vector<Clause> clauses_l, vector<Clause> clauses_s, int ia, int ib){
    if (DEBUG < 0)
        cout << "\n******** building trees a ********" << endl;
    // Clause l should be equal or longer than s
    // Build the tree, and for each l cleause loop s clause
    unordered_map<string, Node*> unify;

    for (int c = 0; c < clauses_l.size(); ++c){
        for (string s : clauses_l[c].literals){
            Node* n = treehelper(s, NULL, unify, 0);
            clauses_l[c].children.push_back(n);
        }
    }

    if (DEBUG < 0)
        cout << "\n******** building trees b ********" << endl;

    for (Clause& c : clauses_s){
        for (string s : c.literals){
            Node* n = treehelper(s, NULL, unify, 0);
            c.children.push_back(n);
        }
    }

    if (DEBUG){
        cout << "\n******** printing trees *******" << endl;
        printtree(clauses_l, unify);
        cout << endl;
        printtree(clauses_s, unify);
        cout << endl;
    }

    if (DEBUG){
        cout << "\n******** unify ********" << endl;
    }

    // Unify!
    int r;
    r = unifyClause(clauses_l[ia], clauses_s[ib], unify);
 
 
    if (DEBUG) cout << "\n--------------------------------" << endl;
    if (r){
        printtree(clauses_l, unify);
        cout << endl;
        printtree(clauses_s, unify);
        cout << endl;
        cout << endl;
    }
    if (DEBUG) cout << "\n--------------------------------" << endl;

    

}

void seprateliterals(vector<string>& clauses_str,  vector<Clause>& clauses){
    for (string s : clauses_str){
        char sign = 1;
        int i = 0;
        Clause c = Clause();
        vector<string> literals;

        if (s[0] == '-'){
            sign = 0;
            ++i;
        }

        string str = "";

        while(s[i] != '('){
            str += s[i];
            ++i;
        }

        c.name = str; 
        c.sign = sign;

        str = "";
        int open_brack = 0;
        int close_brack = 0;

        for(++i; i < s.length(); ++i){
            if (s[i] == '('){
                ++open_brack;
            } else if (s[i] == ')'){
                ++close_brack;
            }  
            
            if ((s[i] == ',' && open_brack - close_brack <= 0 )|| open_brack - close_brack < 0){
                literals.push_back(str);
                str = "";
            } else if (s[i] != ' '){
                str += s[i];
            }
        }
        c.literals = literals;
        clauses.push_back(c);
    }
}

void seprateclauses(string line, vector<string>& sentence, int& counter){

    unordered_map <string, string> dict;
    string s = "";
    bool clause = true;
    bool funconst = false;
    for(int i = 0; i <= line.length(); ++i){
        char cur = line[i];
        
        if (clause && cur == '('){
            clause = false;
        } 

        if (!clause && (cur >= 'A' and cur <= 'Z')){
            funconst = true;
        }

        if (funconst && ( cur == '(' || cur == ',')){
            funconst = false;
        } 
        
        if (cur == '|' || i == line.length()){
            sentence.push_back(s);
            s = "";
            clause = true;
            funconst = false;
        } else if (!clause && !funconst && (cur >= 'a' and cur <= 'z')){
            string v = "";
            while( cur != ' ' && cur != ',' && cur != ')'){
                v += cur;
                ++i;
                cur = line[i];
            }
            if (dict.find(v) != dict.end()){
                v = dict[v];
            } else {
                dict[v] = "v" + to_string(counter);
                v = "v" + to_string(counter);
                ++counter;
            }
            s += v;
            --i;
        } else if (cur != ' ') {
            s += cur;
        }
    }
}

int main(int argc, char** argv){
    string line_a;
    string line_b;
    vector<string> sentence_stra;
    vector<string> sentence_strb;
    vector<Clause> clauses_a;
    vector<Clause> clauses_b;
    int counter = 0;

    for (int i = 0; i < argc; ++i){
        string input = argv[i];
        if (input == "-debug" ){
            DEBUG = 1;
        } 
    }

    getline(cin, line_a);
    getline(cin, line_b);
    seprateclauses(line_a, sentence_stra, counter);
    seprateclauses(line_b, sentence_strb, counter);
    seprateliterals(sentence_stra, clauses_a);
    seprateliterals(sentence_strb, clauses_b);

    if (DEBUG < 0){
        cout << line_a << endl;
        cout << line_b << endl;
        cout << "\n******** clauses strings ********" << endl;
        for (string s : sentence_stra){
            cout << s << endl;
        }
        cout << endl;
        for (string s : sentence_strb){
            cout << s << endl;
        }
        cout << "\n******** clauses ********" << endl;
        for (Clause c : clauses_a){
            cout << c.name << " sign:" << c.sign << " size:" << c.literals.size() << endl;
            cout << "  ";
            for (string s : c.literals){
                cout << s << " : ";
            }
            cout << endl;
        }
        cout << endl;
        for (Clause c : clauses_b){
            cout << c.name << " sign:" << c.sign << " size:" << c.literals.size() << endl;
            cout << "  ";
            for (string s : c.literals){
                cout << s << " : ";
            }
            cout << endl;
        }
    }

    for (int i = 0; i < clauses_a.size(); ++i){
        for(int j = 0; j < clauses_b.size(); ++j){
            buildtree(clauses_a, clauses_b, i, j);
        }
    }
 

    
    return 0;
}