/*
 * CS830 Assignment 4
 * Sai Lekyang
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <stack>
#include <list>
#include <queue>
#include <functional>
#include <math.h> 
#include <algorithm>

using namespace std;

unordered_map<int, int> edge_dupchk;
unordered_map<int, vector <int>> adj_list;

string ALGO;
int COLORS;
int VERTS;
int N_EXP = 0;
int I_RUN = 1;
bool RESTARTS_FLAG = false;

void addEdge(int a, int b){
    adj_list[a].push_back(b);
    adj_list[b].push_back(a);

}

bool checkConstraints(unordered_map<int, int>& s, int id, int color){
    vector <int> neighbors = adj_list[id];
    for (int n : neighbors){
        if (color== s[n]){
            return false;
        }
    }
    return true;
}

// https://stackoverflow.com/
class CompareDist {
public:
    bool operator()(pair<int,int> n1,pair<int,int> n2) {
        return n1.second>n2.second;
    }
};


struct Node{
    int id;
    unordered_map<int, int> state; // vertex, color
    vector<vector <int>> domain; // domain
    unordered_map<int, int>  remaining_nodes;
    vector<int> constraint;
};

int mcv_recursive3(int id, int depth, int color, Node n){
    if (depth > VERTS){
        return 0;
    }

    n.state[id] = color;
    n.remaining_nodes[id] = 0;

    if (depth == VERTS){
        cout << "s col " << COLORS << endl;
        for (int j = 1; j <= VERTS; ++j){
            cout << "l " << j << " " << n.state[j] << endl; 
        }
        return 1;
    }

    ++N_EXP;

    if (RESTARTS_FLAG == true){
        if (N_EXP > 1.0 * VERTS * pow (1.3, I_RUN) ){
            N_EXP = 0;
            ++I_RUN;
            // cout << "restart";
            return 3;
        }
    }
    
    // forward check, update neightbors and constraints
    vector <int> neighbors = adj_list[id];
    for (int ne : neighbors){
        n.domain[ne - 1][color - 1] = 0;
        vector <int> dom = n.domain[ne - 1];
        int sum = 0;
        for (int d : dom){
            sum += d;
        }

        if (sum == 0 && n.remaining_nodes[ne] == 1){
            return 0;
        }

        n.constraint[ne - 1] = sum;
    }

    // get nodes with the least constraints
    priority_queue<pair<int,int>,vector<pair<int,int>>,CompareDist> min_heap; 

    for (int i = 0; i < VERTS; ++i){
        if ( n.remaining_nodes[i + 1] == 1){
            min_heap.push(make_pair(i + 1, n.constraint[i]));
        }
    }

    int t;

    if (RESTARTS_FLAG == true){
        vector<int> ties;
        int min_cont = min_heap.top().second;
        while(!min_heap.empty() && min_heap.top().second == min_cont ){
            int ch = min_heap.top().first;
            ties.push_back(ch);
            min_heap.pop();
        }
        t = ties[rand() % ties.size()];
        // cout << t << endl;
    } else {
        t = min_heap.top().first;

    }
    vector<int> doms = n.domain[t - 1];
    for( int d = 0; d < COLORS; ++d){
        if (doms[d] == 1){
            int r = mcv_recursive3(t, depth + 1, d + 1, n);
            if ( r == 1)
                return 1;
            if (r == 3)
                return 3;
        }
    }

   return 0;
}


void mcv3(){
    unordered_map<int, int> state; // vertex, color
    vector<vector <int>> domain; // domain
    unordered_map<int, int>  remaining_nodes;
    vector<int> constraint;

    for (int i = 1; i <= VERTS; ++i){
        pair <int, int> dom = make_pair(i, COLORS);
        remaining_nodes[i] = 1;
    }

    // create a blank color domain to be used for the root
    for (int v = 0; v < VERTS; ++v){
        vector <int> vert;
        domain.push_back(vert);
        for (int i = 0; i < COLORS; ++i){
            domain[v].push_back(1);
        }
        constraint.push_back(COLORS);
    }


    Node n = Node();
    n.constraint = constraint;
    n.state = state;
    n.domain = domain;
    n.remaining_nodes = remaining_nodes;

    int id = 1;
    int depth = 1;
    int color = 1;
    
    if (RESTARTS_FLAG == true){
        int r = mcv_recursive3(id, depth, color, n);
        while ( r == 3){
            r = mcv_recursive3(id, depth, color, n);
        }
        if (r == 1){
            return;
        }
        cout << "No solution." << endl; 

    } else {
        if (mcv_recursive3(id, depth, color, n) == 1)
            return;
    cout << "No solution." << endl; 
    }
}

int fc_recursive2(int id, int depth, int color, Node n){
    if (depth > VERTS){
        return 0;
    }

    n.state[id] = color;
    n.remaining_nodes[id] = 0;

    if (depth == VERTS){
        cout << "s col " << COLORS << endl;
        for (int j = 1; j <= VERTS; ++j){
            cout << "l " << j << " " << n.state[j] << endl; 
        }
        return 1;
    }

    ++N_EXP;
    
    // forward check, update neightbors and constraints
    vector <int> neighbors = adj_list[id];
    for (int ne : neighbors){
        n.domain[ne - 1][color - 1] = 0;
        vector <int> dom = n.domain[ne - 1];
        int sum = 0;
        for (int d : dom){
            sum += d;
        }

        if (sum == 0 && n.remaining_nodes[ne] == 1){
            return 0;
        }
    }

    vector<int> doms = n.domain[id];
    for( int d = 0; d < COLORS; ++d){
        if (doms[d] == 1){
            int r = fc_recursive2(id + 1, depth + 1, d + 1, n);
            if ( r == 1)
                return 1;
  
        }
    }
   return 0;
}

void fc2(){
    unordered_map<int, int> state; // vertex, color
    vector<vector <int>> domain; // domain
    unordered_map<int, int>  remaining_nodes;

    for (int i = 1; i <= VERTS; ++i){
        pair <int, int> dom = make_pair(i, COLORS);
        remaining_nodes[i] = 1;
    }

    // create a blank color domain to be used for the root
    for (int v = 0; v < VERTS; ++v){
        vector <int> vert;
        domain.push_back(vert);
        for (int i = 0; i < COLORS; ++i){
            domain[v].push_back(1);
        }
    }

    Node n = Node();
    n.state = state;
    n.domain = domain;
    n.remaining_nodes = remaining_nodes;

    int id = 1;
    int depth = 1;

    for (int c = 1; c <= COLORS; ++c){
        if (fc_recursive2(id, depth, c, n) == 1)
                return;
    }  
    cout << "No solution." << endl; 
}

void dfs(){
    list<Node> stack;

    for (int i = 1; i <= COLORS; ++i){
        Node n = Node();
        n.id = 1;
        n.state[1] = i;
        stack.push_front(n);   
    }
    
    while (!stack.empty()){
        Node cur = stack.front();
        stack.pop_front();

        ++N_EXP;  

        if (cur.id > VERTS){
            break;
        }

        for (int i = 1; i <= COLORS; ++i){
            if (checkConstraints(cur.state, cur.id + 1, i)){
                Node n = Node();
                n.id = cur.id + 1;
                n.state = cur.state;
                n.state[n.id] = i;
                if (n.id == VERTS){
                    unordered_map<int, int>::iterator it;
                    bool blank_color = 0;
                    for ( it = n.state.begin(); it != n.state.end(); ++it){
                        if (it->second == 0){
                            blank_color = 1;
                            break;
                        }
                    }
                    stack.push_front(n);   

                    if (!blank_color){ 
                        cout << "s col " << COLORS << endl;
                        for (int j = 1; j <= VERTS; ++j){
                            cout << "l " << j << " " << n.state[j] << endl; 
                        }
                        return;
                    }
                }
                stack.push_front(n);   
            }
        }
    }

    cout << "No solution." << endl; 
}

int main(int argc, char** argv){
    string line;

    ALGO = argv[1];
    COLORS = stoi(argv[2]);

    for (int i = 2; i < argc; ++i){
        string input = argv[i];
        if (input == "-restarts" || input == "-restart" || input == "-r" ){
            RESTARTS_FLAG = true;
        } 
        
    } 

    while (getline(cin,line)){
        stringstream ss(line);
        string word;
        ss >> word;

        if (word == "e") {
            int a;
            int b;
            ss >> a;
            ss >> b;
            addEdge(a, b);
        } else if (word == "p") {
            ss >> word;
            ss >> word;
            VERTS = stoi(word);
        }
    }
    
    if (ALGO == "dfs")
        dfs();
    if (ALGO == "fc")
        fc2();
    if (ALGO == "mcv")
        mcv3();

    cout << N_EXP + 1 << " branching nodes explored." << endl;
    return 0;
}