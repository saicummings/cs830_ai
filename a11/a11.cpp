/*
 * CS830 Assignment 11
 * Sai Lekyang
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <unordered_map>
#include <set>
#include <queue>
#include <math.h>

using namespace std;
typedef pair<double, double> pairs;

double circumfrence(double r){
    return 2 * M_PI * r;
}

double perpendist(double a, double b, double c, pairs p){
    return abs( a*p.first - b*p.second + c ) / (sqrt( a*a + b*b));
}

double distance(double x1, double y1, double x2, double y2) 
{ 
    return sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2)); 
} 

class compare_y {
    public:
        bool operator()(pairs n1,pairs n2) {
            return n1.second>n2.second;
        }   
};
 
int main(int argc, char** argv){
    srand (time(NULL));
    bool DEBUG = false;

    set <pairs> points;

    double height;
    double width;

    for (int i = 0; i < argc; ++i){
        string input = argv[i];
        if (input == "-debug" ){
            DEBUG = true;
        } 
    }

    string line;
    int l = 0;
    while(getline(cin, line)){
        stringstream ss(line);
        if (line[0] != '/'){
            if (l == 0){
                ss >> height; 
                ++l;
            } else if (l == 1) {
                ss >> width;
                ++l;
            } else {
                double x;
                double y;
                ss >> x;
                ss >> y;
                points.insert(make_pair(x,y));
            }
        }
    }

    // cout << points.size() << endl;

    vector <string> best_cirs;
    vector <string> best_lines;
    for (int i = 0; i < 100; ++i){
        vector <pairs> best_inliers;
        string best_cir;
        for (int j = 0; j < 2000; ++j){
            // circle
            if (points.size() < 3) break;

            int a; 
            a = rand() % points.size();
            int b = a; 
            while (b == a) b = rand() % points.size();
            int c = b;
            while (c == a || c == b) c = rand() % points.size();
            auto p1 = points.begin();
            advance(p1, a);
            auto p2 = points.begin();
            advance(p2, b);
            auto p3 = points.begin();
            advance(p3, c);
            double ma = ((*p2).second - (*p1).second)/((*p2).first - (*p1).first);
            double mb = ((*p3).second - (*p2).second)/((*p3).first - (*p2).first);
            double x = ma*mb*((*p1).second - (*p3).second);
            x += mb*((*p1).first + (*p2).first);
            x -= ma*((*p2).first + (*p3).first);
            x /= 2*(mb - ma);

            double y = -(1/ma)*(x - ((*p1).first + (*p2).first)/2) + ((*p1).second + (*p2).second)/2;
            double r = pow((*p1).first - x,2);
            r += pow((*p1).second - y,2);
            r = sqrt(r);

            vector <pairs> inliers;
            
            for (auto it :  points){
                if ( abs(distance(x, y, it.first, it.second) - r) <= 0.8 ){
                    inliers.push_back(it);
                }
            }

            if (inliers.size()/circumfrence(r) > 0.65 ){
                if (inliers.size() > best_inliers.size()){
                    best_inliers = inliers;
                    best_cir = (to_string(x) + " " + to_string(y) + " " + to_string(r));
                }
            }
            //     best_cirs.push_back(to_string(x) + " " + to_string(y) + " " + to_string(r));
            //     for (pairs p : inliers){
            //         points.erase(p);
            //     }
            // }
        }

        if (best_inliers.size() > 0){
            best_cirs.push_back(best_cir);
            for (pairs p : best_inliers){
                points.erase(p);
            }
        }
        
    }  

    // double gminx = width;
    // double gminy = height;            
    // double gmaxx = 0;
    // double gmaxy = 0;

    for(int i = 0; i < 100; ++i){
        vector <pairs> best_inliers;

        for (int j = 0; j < 1000; ++j){
            if (points.size() < 2) break;
            // line
            int a; 
            a = rand() % points.size();
            int b = a; 
            while (b == a) b = rand() % points.size();
            auto p1 = points.begin();
            advance(p1, a);
            auto p2 = points.begin();
            advance(p2, b);

            double m; // slope
            double c; // y-int
            double y = 1;

            priority_queue <pairs> pqueue_x;
            priority_queue<pairs,vector<pairs>, compare_y> pqueue_y;


            double minx = width;
            double miny = height;
            double maxx = 0;
            double maxy = 0;

            if (p1->first == p2->first){
                // x is the same
                for (auto it : points){
                    // iterate over all points and add to inlier if distance is close enough
                    if ( abs(it.first - p1->first) <= 0.75 ){
                        pqueue_x.push(it);
                        pqueue_y.push(it);
                        if (it.first < minx) minx = it.first;
                        if (it.second < miny) miny = it.second;
                        if (it.first > maxx) maxx = it.first;
                        if (it.second > maxy) maxy = it.second;
                    }
                }

            } else if (p1->second == p2->second ){
                // y is the same
                for (auto it : points){
                    // iterate over all points and add to inlier if distance is close enough
                    if ( abs(it.second - p1->second) <= 0.75 ){
                        pqueue_x.push(it);
                        pqueue_y.push(it);
                        if (it.first < minx) minx = it.first;
                        if (it.second < miny) miny = it.second;
                        if (it.first > maxx) maxx = it.first;
                        if (it.second > maxy) maxy = it.second;
                    }
                }
            } else {
                m = (p1->second - p2->second)/(p1->first - p2->first); // slope
                c = p1->second - m * p1->first; // y-int
                for (auto it : points){
                    // iterate over all points and add to inlier if distance is close enough
                    if ( perpendist(m, y, c, it) <= 0.75 ){
                        pqueue_x.push(it);
                        pqueue_y.push(it);
                        if (it.first < minx) minx = it.first;
                        if (it.second < miny) miny = it.second;
                        if (it.first > maxx) maxx = it.first;
                        if (it.second > maxy) maxy = it.second;
                    }
                }
            }

            if (pqueue_x.empty()) continue;

            // if (minx < gminx) gminx = minx;
            // if (miny < gminy) gminy = miny;
            // if (maxx > gmaxx) gmaxx = maxx;
            // if (maxy > gmaxy) gmaxy = maxy;

            // need to check which queue to use
            double diffy = abs(miny - maxy);
            double diffx = abs(minx - maxx);

            vector <pairs> inliers;
            if (diffx >= diffy){
                pairs cur = pqueue_x.top();
                pqueue_x.pop();
                inliers.push_back(cur);

                // list of points sorted by x
                while(!pqueue_x.empty()){
                    // cout << "x " << cur.first << " " << cur.second << endl;
                    pairs next = pqueue_x.top();
                    pqueue_x.pop();

                    if (distance(cur.first, cur.second, next.first, next.second) > 4 || pqueue_x.empty()){
                        // end of segment
                        int inliers_size = inliers.size();
                        if (inliers_size > 1) {
                            double line_dist = distance(inliers[0].first, inliers[0].second, inliers[inliers_size-1].first, inliers[inliers_size-1].second);
                            if (inliers.size() / line_dist >= 0.7){
                                // check density
                                if ( inliers_size > best_inliers.size() ){
                                    best_inliers = inliers;
                                }
                            }
                        }
                        inliers.clear();
                    }

                    cur = next;
                    inliers.push_back(cur);
                }
            } else {
                pairs cur = pqueue_y.top();
                pqueue_y.pop();
                inliers.push_back(cur);

                // list of points sorted by y
                while(!pqueue_y.empty()){
                    // cout << "y " << cur.first << " " << cur.second << endl;
                    pairs next = pqueue_y.top();
                    pqueue_y.pop();

                    if (distance(cur.first, cur.second, next.first, next.second) > 4 || pqueue_y.empty()){
                        // end of segment
                        int inliers_size = inliers.size();
                        if (inliers_size > 1) {
                            double line_dist = distance(inliers[0].first, inliers[0].second, inliers[inliers_size-1].first, inliers[inliers_size-1].second);
                            if (inliers.size() / line_dist >= 0.7){
                                // check density
                                if ( inliers_size > best_inliers.size() ){
                                    best_inliers = inliers;
                                }
                            }
                        }
                        inliers.clear();
                    }

                    cur = next;
                    inliers.push_back(cur);
                }
            }

            // cout << endl;
        }

        if(best_inliers.size() > 0){
            // for (pairs p : best_inliers){
            //     cout << p.first << " " << p.second << endl;
            // }

            best_lines.push_back(to_string(best_inliers[0].first) + " " + to_string(best_inliers[0].second) + " " + to_string(best_inliers[best_inliers.size()-1].first) + " " + to_string(best_inliers[best_inliers.size()-1].second));
            for (pairs p : best_inliers){
                points.erase(p);
            }
            // cout << gminx << endl;
            // cout << gminy << endl;
            // cout << gmaxx << endl;
            // cout << gmaxy << endl;
        }
    }

    cout << "number of circles: " << best_cirs.size() << endl;
    for (string s : best_cirs)
        cout << s << endl;
    cout << "number of lines: " << best_lines.size() << endl;
    for (string s : best_lines)
        cout << s << endl;


}