#include <iostream>
#include <sstream>
#include <fstream>
#include <unordered_map>
#include <algorithm>
#include "hashtup.h"
#include "gridworld.h"
#include "agent_ca.h"

using namespace std;
typedef GridWorld::State State;
typedef tuple<int, int, int> tup;

struct compare_cost {
    bool operator()(AgentCA* lhs, AgentCA* rhs){
        return lhs->optimal() > rhs->optimal();
    }
};

int main(int argc, char** argv){
    GridWorld gridworld;
    vector<AgentCA*> agents;
    priority_queue<AgentCA*, vector<AgentCA*>, compare_cost> order;
    unordered_set <tup, hash<tup>> reserved_table;
    int max_agents = 1;
    bool custom_ordering = true;

    string algorithm = argv[4];

    ifstream map_file(argv[1]);
    if (map_file.is_open() && map_file.good()) {
        gridworld = GridWorld(map_file);
    } else {
        cout << "Failed to open map file.";
        exit(1);
    }



    ifstream scen_file(argv[2]);
    if (scen_file.is_open() && scen_file.good()) {
        if (argc > 3){
            max_agents = stoi(argv[3]);
        }
        string line;
        int r = 0;
        while (getline(scen_file, line)){
            string word;
            if (r > 0){
                stringstream ss(line);
                int sx, sy, gx, gy;
                ss >> word;
                ss >> word;
                ss >> word;
                ss >> word;
                ss >> sx;
                ss >> sy;
                ss >> gx;
                ss >> gy;
                State start = gridworld.make_state(sx, sy, 0, 'S');
                State goal = gridworld.make_state(gx, gy, 0, 'G');
                if (custom_ordering)
                    order.push(new AgentCA(gridworld, reserved_table, algorithm, start, goal));
                else
                    agents.push_back(new AgentCA(gridworld, reserved_table, algorithm, start, goal));

            } 
            if (r++ >= max_agents) break;
        }
    } else {
        cout << "Failed to open map file.";
        exit(1);
    }
    map_file.close();
    scen_file.close();
    
    int id = 0;

    if (custom_ordering)
        while (!order.empty()){
            order.top()->set_id(id++);
            agents.push_back(order.top());
            order.pop();
        }


    vector <vector<char>> plan;

    if (algorithm == "whca") {
        while(true){
            bool finish = true;
            for (AgentCA* a : agents){
                // cout << a->get_id() << endl;
                int s = a->solve();
                if (s == -1){
                    cout << "Failed to solve." << endl;
                    exit(1);
                } else {
                    finish = finish && (s == 1);
                }
            }
            if (finish) break;
        }
    } else {
        for (AgentCA* a : agents){
            if (a->solve() == -1){
                cout << "Failed to solve." << endl;
                exit(1);
            }
            
        }
    }

    int longest_path = 0;
    int comma = 0;
    for (AgentCA* a : agents){
        if(comma++ > 0) cout << ',';
        State start = a->start_state();
        cout << start.x << " " << start.y;
    }

    int total_nodes_gen = 0;
    int total_nodes_exp = 0;
    int total_g = 0;

    for (AgentCA* a : agents){
        plan.push_back(a->get_plan());
        longest_path = max(longest_path, (int)plan[plan.size() - 1].size());
        total_nodes_gen += a->get_node_gen();
        total_nodes_exp += a->get_node_exp();
        total_g += a->get_g();
    }

    cout << endl;
    comma = 0;
    for (AgentCA* a : agents){
        State goal = a->goal_state();
        if(comma++ > 0) cout << ',';
        cout << goal.x << " " << goal.y;
    }
    cout << endl;

    for (int t = 0; t < longest_path; ++t){
        comma = 0;

        for (int i = 0; i < plan.size(); ++i){
            if(comma++ > 0) cout << ',';
            if (t >= plan[i].size()){
                cout << 'W';
            } else {
                cout << plan[i][t];
            } 
        } cout << endl;
    }

    cout << total_nodes_gen << endl;
    cout << total_nodes_exp << endl;
    cout << total_g << endl;
    exit(0);

    cout << "# Gen: " << total_nodes_gen << endl;
    cout << "# Exp: " << total_nodes_exp << endl;
    cout << "# Cost: " << total_g << endl;
}