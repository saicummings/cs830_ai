#include <iostream>
#include <sstream>
#include <fstream>
#include <unordered_map>
#include <algorithm>
#include "hashtup.h"
#include "gridworld.h"
#include "agent_cbs.h"

using namespace std;
typedef GridWorld::State State;
typedef tuple<int, int, int> tup;

int main(int argc, char** argv){
    GridWorld gridworld;
    vector<State> starts;
    vector<State> goals;
    int max_agents = 1;

    ifstream map_file(argv[1]);
    if (map_file.is_open() && map_file.good()) {
        gridworld = GridWorld(map_file);
    } else {
        cout << "Failed to open map file.";
        exit(1);
    }

    AgentCBS cbs(gridworld);
    ifstream scen_file(argv[2]);
    if (scen_file.is_open() && scen_file.good()) {
        if (argc > 3){
            max_agents = stoi(argv[3]);
        }
        string line;
        int r = 0;
        while (getline(scen_file, line)){
            string word;
            if (r > 0){
                stringstream ss(line);
                int sx, sy, gx, gy;
                ss >> word;
                ss >> word;
                ss >> word;
                ss >> word;
                ss >> sx;
                ss >> sy;
                ss >> gx;
                ss >> gy;
                State start = gridworld.make_state(sx, sy, 0, 'S');
                State goal = gridworld.make_state(gx, gy, 0, 'G');
                starts.push_back(start);
                goals.push_back(goal);
                cbs.add_agent(start, goal);
            } 
            if (r++ >= max_agents) break;
        }
    } else {
        cout << "Failed to open map file.";
        exit(1);
    }
    
    map_file.close();
    scen_file.close();

    if (cbs.solve() == -1){
        cout << "Failed to solve.";
        exit(1);
    }

    vector <vector<char>> plan = cbs.get_plan();
    int longest_path = 0;
    for (vector<char>& a : plan){
        longest_path = max(longest_path, (int)a.size());
    }

    // cout << cbs.get_node_gen() << endl;
    // cout << cbs.get_node_exp() << endl;
    // cout << cbs.get_g() << endl;
    // cout << longest_path << endl;
    // exit(0);

    int comma = 0;
    for(State s : starts){
        if(comma++ > 0) cout << ',';
        cout << s.x << " " << s.y;
    }
    cout << endl;
    comma = 0;
    for(State s : goals){
        if(comma++ > 0) cout << ',';
        cout << s.x << " " << s.y;
    }
    cout << endl;

    for (int t = 0; t < longest_path; ++t){
        comma = 0;

        for (int i = 0; i < plan.size(); ++i){
            if(comma++ > 0) cout << ',';
            if (t >= plan[i].size()){
                cout << 'W';
            } else {
                cout << plan[i][t];
            } 
        } cout << endl;
    }

    int total_nodes_gen = 0;
    int total_nodes_exp = 0;

    cout << "# Gen: " << cbs.get_node_gen() << endl;
    cout << "# Exp: " << cbs.get_node_exp() << endl;
    cout << "# Cost: " << cbs.get_g() << endl;

}