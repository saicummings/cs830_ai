#!/usr/bin/env python
# With the help of to get started:
# https://gist.github.com/ohsqueezy/6780206


import sys  
import os
import pygame
import time
from pygame.locals import *
from sets import Set
from random import randint
from subprocess import Popen, PIPE
import argparse

parser = argparse.ArgumentParser(description="")
parser.add_argument("executable")
parser.add_argument("map")
parser.add_argument("scen")
parser.add_argument("agents")
if sys.argv[1] == "cca":
    parser.add_argument("algorithm")
args = parser.parse_args()
params = []
params += [args.map]
params += [args.scen]
params += [args.agents]
if sys.argv[1] == "cca":
    params += [args.algorithm]
cmd = [" ".join(["./" + args.executable] + params)]
process = Popen(cmd, stdin=PIPE, stdout=PIPE, shell=True)
output = process.communicate()[0].split('\n')

filepath = args.map
if not os.path.isfile(filepath):
    print("File path {} does not exist. Exiting...".format(filepath))
    sys.exit()

grid = []
map_height = 0
map_width = 0
with open(filepath) as fp:
    next(fp)
    map_height = int(next(fp).split(' ')[1])
    map_width = int(next(fp).split(' ')[1])
    next(fp)
    for line in fp:
        row = []
        for c in line:
            if c == '@' or c == 'T':
                row.append(0)
            elif c == '.':
                row.append(1)
        grid.append(row)

# player_color = (255, 0, 0)

resolution = (map_width * 28, map_height * 28)
cell_margin = 0
cell_colors = (0, 0, 0), (255, 255, 255)
player_size = 28
current_position = []
player_colors = []
goals = []
path_char = '.'
path_set = set([])

print_path = False

def main():
    pygame.init()
    screen = pygame.display.set_mode(resolution)
    screen.fill(cell_colors[1])
    player = [] # pygame.font.Font(None, player_size).render(player_char, False, player_color)
    path = [] # pygame.font.Font(None, player_size).render(path_char, False, player_color)
    goal = [] # pygame.font.Font(None, player_size).render('X', False, player_color)

    draw_maze(screen)
    pygame.display.update()

    total = -1
    # for line in sys.stdin:
    for line in output:
        total = total + 1
            
        if line[0] == '#':
            print(line)
            continue

        # done = True
        line = line.strip()
        agent_moves = line.split(',')

        
        for a in agent_moves:
            if a != 'W':
                done = False
        
        if done:
            break

        state_t = pygame.font.Font(None, player_size).render(str(total), 1, (255, 10, 10))

        if len(current_position) == 0:
            for i, a in enumerate(agent_moves):
                startx = int(a.split(' ')[0])
                starty = int(a.split(' ')[1])
                current_position.append([startx, starty])
                color = randcolor()
                # player.append(pygame.font.Font(None, player_size).render(str(i), False, color))
                goal.append(pygame.font.Font(None, player_size).render('x', False, color))
                player.append(pygame.font.Font(None, player_size).render('@', False, color))
                if (print_path): 
                    r = pygame.font.Font(None, player_size).render(path_char, False, color)
                    r.set_alpha(20)  
                    path.append(r)
                    path_set.append(set([]))
                

        elif len(goals) == 0:
            draw_maze(screen)
            for i, a in enumerate(agent_moves):
                goalx = int(a.split(' ')[0])
                goaly = int(a.split(' ')[1])
                goals.append([goalx, goaly])
                draw_goals(i, goal[i], screen)
                draw_player(i, player[i], screen)
            pygame.display.update()
            
            

        else:
            smooth = 8
            inc = 1.0/smooth
            for s in xrange(smooth):
                draw_maze(screen)
                for i, action in enumerate(agent_moves):
                    if action == 'U':
                        move(i, 0, -inc)
                    elif action == 'R':
                        move(i, inc, 0)
                    elif action == 'D':
                        move(i, 0, inc)
                    elif action == 'L':
                        move(i, -inc, 0)
                    if (print_path): draw_path(i, path[i], screen)
                    draw_goals(i, goal[i], screen)
                    draw_player(i, player[i], screen)
                draw_time(state_t, screen)
                pygame.display.update()
 
        
    print(total)
    time.sleep(100)

def randcolor():
    return (randint(0, 255) / 15 * 15, randint(0, 255) / 15 * 15, randint(0, 255) / 15 * 15)

def draw_goals(aid, goal, screen):
    rect = goal.get_rect()
    rect.center = get_cell_rect(goals[aid], screen).center
    screen.blit(goal, rect)

def draw_maze(screen):
    for row in xrange(len(grid[0])):
        for column in xrange(len(grid)):
            screen.fill(cell_colors[grid[column][row]], get_cell_rect((row, column), screen))

def get_cell_rect(coordinates, screen):
    row, column = coordinates
    cell_width = screen.get_width() / len(grid)
    adjusted_width = cell_width - cell_margin
    return pygame.Rect(row * cell_width + cell_margin / 2,
                       column * cell_width + cell_margin / 2,
                       adjusted_width, adjusted_width)

def draw_time(t, screen):
    rect = t.get_rect()
    screen.blit(t, rect)

def draw_player(aid, player, screen):
    rect = player.get_rect()
    rect.center = get_cell_rect(current_position[aid], screen).center
    screen.blit(player, rect)

def draw_path(aid, path, screen):
    rect = path.get_rect()
    for pos in path_set[aid]:
        rect.center = get_cell_rect(list(pos), screen).center
        screen.blit(path, rect)

def move(aid, dx, dy):
    x, y = current_position[aid]
    path_set.add(tuple([x,y]))
    nx, ny = x + dx, y + dy
    current_position[aid][0] = nx
    current_position[aid][1] = ny
    # if nx >= 0 and nx < len(grid) and ny >= 0 and ny < len(grid[0]) and grid[ny][nx]:
    #     current_position[aid][0] = nx
    #     current_position[aid][1] = ny
    # else: 
    #     player_color = (0, 0, 0)

if __name__ == "__main__":
    main()
    pygame.quit()