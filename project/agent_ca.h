#pragma once
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <queue>
#include <vector>
#include "hashtup.h"
#include "gridworld.h"
#include "node.h"
#include "rra.h"

using namespace std;

class AgentCA{
    typedef GridWorld::State State;
    typedef tuple<int, int, int> tup;
    typedef shared_ptr<Node> node_ptr;
 
    public:
    AgentCA(GridWorld& g, unordered_set<tup, hash<tup>>& rt, string s, State start, State goal) : 
                    world(g), reserved_table(rt), algorithm(s), rra(RRA(g)), start(start), goal(goal){
            optimal_cost = rra.init(start, goal);
            current = start;
            tup xyt = make_tuple(current.x, current.y, current.t);
            reserved_table.insert(xyt);
    }

    void set_window(int w){
        window = w;
    }

    void set_tlimit(int t){
        tlimit = t;
    }

    void set_id(int d){
        id = d;
    }

    int get_id(){
        return id;
    }

    int get_g(){
        return g;
    }

    int optimal(){
        return optimal_cost;
    }

    State start_state(){
        return start;
    }

    State goal_state(){
        return goal;
    }

    vector <char>& get_plan(){
        return plan;
    }

    int get_node_exp(){
        return node_exp + rra.get_node_exp();
    }

    int get_node_gen(){
        return node_gen + rra.get_node_gen();
    }
    
    double heuristic(const State& start, const State& goal){
        if (algorithm == "rra" || algorithm == "whca"){
            return rra.abstract_dist(start, goal);
        } else {
            return world.heuristic(start, goal);
        }
    }

    int edge_cost(int g, char w, State p ){
        if (p.x == goal.x && p.y == goal.y && w == 'W') return g;
        return g + 1;
    }

    int solve(){
        node_ptr n (new Node(NULL, current, g, heuristic(current, goal)));
        if (algorithm == "wcha" && g == 0) plan.push_back(current.label);
        int window_limit = current.t + window;
        priority_queue<node_ptr, vector<node_ptr>, Node::compare_f > open_list;
        unordered_map <tup, node_ptr, hash<tup>> node_map;
        open_list.push(n);
        
        // DEBUG AREA
        bool DEBUG = false;
        int DEBUG_ID = 0;
        if (DEBUG && id == DEBUG_ID){ 
            cout << "\n_______" << window_limit << endl;
            cout << n->state.x << " " << n->state.y << " " << n->state.label << endl;
        }

        while (!open_list.empty()){
            node_ptr cur(open_list.top());
            
            if (DEBUG && id == DEBUG_ID){
                cout << "\033[36m Cur x=" << cur->state.x << " y=";
                cout << cur->state.y << " t=" << cur->state.t;
                cout << " " << cur->state.label << " g=" << cur->g << " h=";
                cout << cur->h << " f=" << cur->g + cur->h <<  "\033[39m" << endl;
            }

            if (cur->state.t > current.t + tlimit){
                cout << "Limit reached" << endl;
                break;
            }
            if (algorithm == "whca"){
                if (cur->state.t > window_limit){
                    vector<node_ptr> path;

                    while(cur){
                        path.push_back(cur);
                        cur = cur->parent;
                    }

                    // commit half-way
                    int i;
                    for (i = path.size() - 2; i > window/2; --i){
                        State s = path[i]->state;
                        tup xyt = make_tuple(s.x, s.y, s.t);
                        
                        if ( DEBUG && id == DEBUG_ID){ 
                            cout << path[i]->g << " " << path[i]->h << " " << s.label << endl; 
                        }
                        
                        reserved_table.insert(xyt);
                        plan.push_back(s.label);
                    }
                    current =  path[i]->state;
                    State s = path[i]->state;
                    tup xyt = make_tuple(s.x, s.y, s.t);
                    plan.push_back(s.label);

                    reserved_table.insert(xyt);
                    g = path[i]->g;  

                    int at_goal = 0;
                    if(current.x == goal.x && current.y == goal.y){
                        at_goal = 1;
                    }
                    return at_goal;
                }
            } else {
                if(cur->state.x == goal.x && cur->state.y == goal.y){
                    if (cur->state.t >= tlimit){
                        g = cur->g;
                        while(cur){
                            State s = cur->state;
                            tup xyt = make_tuple(s.x, s.y, s.t);
                            plan.push_back(s.label);
                            reserved_table.insert(xyt);
                            cur = cur->parent;
                        }
                        reverse(plan.begin(), plan.end());
                        return 1;
                    } 
                }
            }
            ++node_exp;
            open_list.pop();
            cur->open = false;
            vector<State> children = world.successors(cur->state);
            node_gen += children.size();
            for (State child : children){
                // check for other agents being an obstacle
                tup res_tup = make_tuple(child.x, child.y, child.t - 1);
                if (reserved_table.find(res_tup) != reserved_table.end()) {
                    if (algorithm != "whca"){
                        continue;
                    } else if (child.x != cur->state.x || child.y != cur->state.y){
                        continue;
                    }
                }

                // temporal space is reserved
                tup child_tup = make_tuple(child.x, child.y, child.t);
                if (reserved_table.find(child_tup) != reserved_table.end()){
                    continue;
                }   

                auto it = node_map.find(child_tup);
                if (it == node_map.end()){
                    // new node discovered
                    int cost;
                    if (algorithm == "whca" && cur->state.t == window_limit){
                        cost = heuristic(cur->state, goal);
                    } else {
                        cost = edge_cost(cur->g, child.label, cur->state);
                    }
                    node_ptr child_node (new Node(cur, child, cost, heuristic(child, goal)));
                    
                    if (DEBUG && id == DEBUG_ID){
                        cout << "child " << child_node->state.x<< " "<< child_node->state.y << " ";
                        cout << child_node->state.label << " " << child_node->g << " " << child_node->h <<endl;
                    }

                    open_list.push(child_node);
                    node_map[child_tup] = child_node;
                } else {
                    // check to see if it is in the conceptual open list
                    node_ptr child_node = it->second;
                    
                    if (DEBUG && id == DEBUG_ID){        
                        cout << "child1 " << child_node->state.x<< " "<< child_node->state.y << " ";
                        cout << child_node->state.label << " " << child_node->g << " " << child_node->h <<endl;
                    }

                    if (child_node->open && cur->g < child_node->g){
                        // found a better path to child
                        child_node->parent = cur;
                        child_node->g = edge_cost(cur->g, child.label, cur->state);
                        child_node->state = child;
                        open_list.push(child_node);
                    }
                }
            } 
        }
        return -1;
    }
	protected:
		GridWorld& world;
        unordered_set <tup, hash<tup>>& reserved_table;
        vector <char> plan;
        State current;
        State start;
        State goal;
        int window = 32;
        int tlimit = 200;
        int id = -1;
        int g = 0;
        int node_exp = 0;
        int node_gen = 0;
        string algorithm;
        RRA rra;
        int optimal_cost;
};