#pragma once
#include <ostream>
#include <sstream>
#include <vector>
#include <iostream>
#include "hashtup.h"

using namespace std;


class GridWorld {
    public: 
    class State {
        public:
            State(){}

            State(int x, int y, int t, char l) : x(x), y(y), t(t), label(l){ 
            }

            int t;
            int x;
            int y;
            char label;
    };

    GridWorld(){}

    GridWorld(std::istream& input) {
		string line;
        int r = 0;
		while(getline(input, line)){
            string word;
            stringstream ss(line);
            if (r == 1){
                ss >> word;
                ss >> map_h;
            } else if (r == 2){
                ss >> word;
                ss >> map_w;
            } else if (r > 3){
                map.push_back(line);
            }
            ++r;
        }
    }

    double heuristic(const State& state, const State& goal_state) {
        return abs(state.x - goal_state.x) + abs(state.y - goal_state.y);
    }

    State make_state(int x, int y, int t, char c){
        return State(x, y, t, c);
    }

    void add_obstacle(int x, int y){
        map[y][x] = '@'; // row, col
    }

    vector<State> successors(const State& state) {
        vector<State> successors;
        int row = state.y;
        int col = state.x;
        int t = state.t;

        // W
        State s = State(col, row, t + 1, 'W');
        successors.push_back(s);

        // U
        if (row != 0 && (map[row - 1][col] == '.' || map[row - 1][col] == 'G')){
            if (state.label != 'D'){
                State s = State(col, row - 1, t + 1,  'U');
                successors.push_back(s);
            }
        }

        // D
        if (row != map_h - 1 && (map[row + 1][col] == '.' || map[row + 1][col] == 'G')){
            if (state.label != 'U'){
                State s = State(col, row + 1, t + 1, 'D');
                successors.push_back(s);
            }
        }

        // L
        if (col != 0 && (map[row][col - 1] == '.' || map[row][col - 1] == 'G')){
            if (state.label != 'R'){
                State s = State(col - 1, row, t + 1, 'L');
                successors.push_back(s);
            }
        }
        
        // R
        if (col != map_w - 1 && (map[row][col + 1] == '.' || map[row][col + 1] == 'G')){
            if (state.label != 'L'){
                State s = State(col + 1, row, t + 1, 'R');
                successors.push_back(s);
            }
        }

        return successors;
    }

    protected:
        vector <string> map;
        int map_h;
        int map_w;
};