#pragma once
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <queue>
#include <vector>
#include "hashtup.h"
#include "gridworld.h"
#include "node.h"

using namespace std;

class AgentCBS{
    typedef GridWorld::State State;
    typedef tuple<int, int, int> loc;
    typedef tuple<int, tuple<int, int, int>> conflict; // ai, aj, x, y, t
    typedef shared_ptr<Node> node_ptr;    

    private:
    struct CBSNode {
        vector<vector<loc>> solution; // [agent][solution]
        vector<vector<char>> actions; // [agent][actions]
        vector<int> g; // [agent][actions]
        vector<unordered_set<loc, hash<loc>>> constraints; // [agent][constraints]
        int cost;
        bool operator<(const CBSNode& n) const {
            return cost > n.cost;
        }
    };

    struct agent {
        public:
        State start;
        State goal;
        int id;
        agent(State s, State g, int id): start(s), goal(g), id(id){}
    };
    
    public:
    AgentCBS(GridWorld& g): world(g){}

    int get_node_exp(){
        return node_exp;
    }

    int get_node_gen(){
        return node_gen;
    }

    int get_g(){
        return total_g;
    }

    bool validate(CBSNode& highlevel, vector<conflict>& con){
        int longest_path = 0;
        for (vector<char>& a : highlevel.actions){
            longest_path = max(longest_path, (int)a.size());
        }
        for (int t = 0; t < longest_path - 1; ++t){
            // checking agents at the same temporal location
            for (int i = 0; i < agents.size(); ++i){
                // fill the rest of path with wait if shorter than longest
                loc n;
                if (highlevel.solution[i].size() <= t){
                    n = highlevel.solution[i].back();
                    get<2>(n) = t;
                    // highlevel.solution[i].push_back(n);
                    // highlevel.actions[i].push_back('W');
                } else {
                    n = highlevel.solution[i][t];
                }

                for (int j = i + 1; j < agents.size(); ++j){
                    loc m;
                    if (highlevel.solution[j].size() <= t){
                        m = highlevel.solution[j].back();
                        get<2>(m) = t;
                        // highlevel.solution[i].push_back(m);
                        // highlevel.actions[i].push_back('W');
                    } else {
                        m = highlevel.solution[j][t];
                    }

                    if (n == m){
                        // cout << i << " " << n << " " << j << " " << m << endl;
                        con.push_back(make_tuple(i, n));
                        con.push_back(make_tuple(j, n));
                        return false;
                    }
                }
            }

            // checking for surround agents
            for (int i = 0; i < agents.size(); ++i){
                loc n = highlevel.solution[i][t];
                loc np = highlevel.solution[i][t + 1];

                for (int j = i + 1; j < agents.size(); ++j){
                    loc m = highlevel.solution[j][t];
                    loc mp = highlevel.solution[j][t + 1];

                    if (get<0>(n) == get<0>(mp) && get<1>(n) == get<1>(mp)  &&
                                     get<0>(np) == get<0>(m) && get<1>(np) == get<1>(m)){
                        // agent j want to move to agent i loc in t + 1 and vice versa
                        con.push_back(make_tuple(i, n));
                        con.push_back(make_tuple(j, m));
                        return false;
                    }
                }
            }
        }
        return true;
    }

    vector<vector<char>>& get_plan(){
        return final_plan;
    }

    int solve(){
        CBSNode r;
        r.solution.resize(agents.size());
        r.constraints.resize(agents.size());
        r.actions.resize(agents.size());
        r.g.resize(agents.size());
        r.cost = 0;

        // find individual paths using low level
        for (int i = 0; i < agents.size(); ++i){
            // SIC: sum of individual cost
            if (astar(r, agents[i], i) == -1) {
                return -1;
            }
            // r.longest_path  = max(r.longest_path, (int)r.actions[i].size());
        }


        open_list.push(r);
        
        while (!open_list.empty()) {
            // lowest solution cose
            CBSNode p = open_list.top();
            open_list.pop();

            ++highnode_exp;

            // validate the paths in p until conflict occurs
            vector<conflict> con;
            if (validate(p, con)){
                final_plan = p.actions;
                total_g = p.cost;
                return 1;
            }

            for (conflict c : con){
                CBSNode n = p;
                int i = get<0>(c);
                n.constraints[i].insert(get<1>(c));
                if (astar(n, agents[i], i) == 1) {
                    open_list.push(n);
                }
            }
        }
        return -1;
    }

    void add_agent(State s, State g){
        int id = agents.size();
        agents.push_back(agent(s, g, id));
    }

    int edge_cost(int g, char w, State p, State goal){
        if (p.x == goal.x && p.y == goal.y && w == 'W') return g;
        return g + 1;
    }

    // used for low level search
    int astar(CBSNode& highlevel, agent& a, int id){
        // cout << "ID: " << id << endl;

        State start = a.start;
        State goal = a.goal;
        priority_queue<node_ptr, vector<node_ptr>, Node::compare_f > open_list;
        unordered_map <loc, node_ptr, hash<loc>> node_map;

        node_ptr s (new Node(NULL, start, 0, world.heuristic(start, goal)));
        open_list.push(s);

        while (!open_list.empty()){ 
            node_ptr cur(open_list.top());
            if (cur->state.x == goal.x && cur->state.y == goal.y){
            // if (cur->state.x == goal.x && cur->state.y == goal.y && cur->state.t == tlimit){
                    highlevel.cost += cur->g - highlevel.g[id];
                    highlevel.g[id] = cur->g;
                    vector<loc> solution;
                    vector<char> actions;
                    while(cur){
                        State s = cur->state;
                        loc xyt = make_tuple(s.x, s.y, s.t);
                        actions.push_back(s.label);
                        solution.push_back(xyt);
                        cur = cur->parent;
                    }
                    reverse(actions.begin(), actions.end());
                    highlevel.actions[id] = actions;

                    reverse(solution.begin(), solution.end());
                    highlevel.solution[id] = solution;
                    return 1;
            }

            ++node_exp;
            open_list.pop();
            cur->open = false;
            vector<State> children = world.successors(cur->state);
            node_gen += children.size();
            for (State child : children){
                loc child_tup = make_tuple(child.x, child.y, child.t);

                if (highlevel.constraints[id].find(child_tup) != 
                                highlevel.constraints[id].end()){
                    if (DEBUG) cout << "astar conflict " << child_tup << endl;
                    continue;
                } 

                auto it = node_map.find(child_tup);
                if (it == node_map.end()){
                    // new node discovered
                    node_ptr child_node (new Node(cur, child, edge_cost(cur->g, 
                                    child.label, cur->state, goal), world.heuristic(child, goal)));
                    open_list.push(child_node);
                    node_map[child_tup] = child_node;
                } else {
                    // check to see if it is in the conceptual open list
                    node_ptr child_node = it->second;
                    if (child_node->open && cur->g < child_node->g){
                        child_node->parent = cur;
                        child_node->g = edge_cost(cur->g, child.label, cur->state, goal);
                        child_node->state = child;
                        open_list.push(child_node);
                    }
                }
            }
        }

        cout << "No solution" << endl;
        return -1; 
    }

	protected:
    GridWorld& world;
    vector <agent> agents;
    vector<vector<char>> final_plan;
    priority_queue<CBSNode> open_list;
    // node_ptr current;
    int tlimit = 50;
    int total_g = 0;
    int node_exp = 0;
    int node_gen = 0;
    int highnode_exp = 0;
    int highnode_gen = 0;
    bool DEBUG = false;
};