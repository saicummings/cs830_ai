// https://www.nullptr.me/
// Sarang Baheti
// http://cpplove.blogspot.com/2012/07/printing-tuples.html
#pragma once
#include <tuple>
#include <utility> 
#include <iostream>
#include <ostream>


template<std::size_t> struct int_{};

template <class Tuple, size_t Pos>
std::ostream& print_tuple(std::ostream& out, const Tuple& t, int_<Pos> ) {
  out << std::get< std::tuple_size<Tuple>::value-Pos >(t) << ',';
  return print_tuple(out, t, int_<Pos-1>());
}

template <class Tuple>
std::ostream& print_tuple(std::ostream& out, const Tuple& t, int_<1> ) {
  return out << std::get<std::tuple_size<Tuple>::value-1>(t);
}

template <class... Args>
std::ostream& operator<<(std::ostream& out, const std::tuple<Args...>& t) {
  out << '('; 
  print_tuple(out, t, int_<sizeof...(Args)>()); 
  return out << ')';
}



template<typename T>
inline void hash_combine(std::size_t& seed, const T& val){
    std::hash<T> hasher;
    seed ^= hasher(val) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

template<class... TupleArgs>
struct std::hash<std::tuple<TupleArgs...>>{
private:
    //  This is a termination condition
    //  N == sizeof...(TupleTypes)
    //
    template<size_t Idx, typename... TupleTypes>
    inline typename std::enable_if<Idx == sizeof...(TupleTypes), void>::type
    hash_combine_tup(size_t& seed, const std::tuple<TupleTypes...>& tup) const{
    }

    //  This is the computation function
    //  Continues till condition N < sizeof...(TupleTypes) holds
    //
    template<size_t Idx, typename... TupleTypes>
    inline typename std::enable_if <Idx < sizeof...(TupleTypes), void>::type
    hash_combine_tup(size_t& seed, const std::tuple<TupleTypes...>& tup) const{
        hash_combine(seed, std::get<Idx>(tup));

        //  On to next element
        hash_combine_tup<Idx + 1>(seed, tup);
    }

public:
    size_t operator()(const std::tuple<TupleArgs...>& tupleValue) const{
        size_t seed = 0;
        //  Begin with the first iteration
        hash_combine_tup<0>(seed, tupleValue);
        return seed;
    }
};

