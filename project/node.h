#pragma once
#include "gridworld.h"
#include <memory>

using namespace std;

struct Node	{  
    typedef GridWorld::State State;
    typedef shared_ptr<Node> node_ptr;

    public:
    double g;
    double h;
    bool open;
    State state;
    node_ptr parent;
    Node(node_ptr parent, State state, double g, double h) : 
                    parent(parent), state(state), g(g), h(h){
        open = true;
    }

    struct compare_f {
        bool operator()(const node_ptr lhs, const node_ptr rhs){
            return lhs->g + lhs->h  > rhs->g + rhs->h;
        }
    };
};