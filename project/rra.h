#pragma once
#include <memory>
#include <unordered_map>
#include <queue>
#include <vector>
#include <limits>
#include "gridworld.h"
#include "hashtup.h"
#include "node.h"

using namespace std;

class RRA{
    typedef GridWorld::State State;
    typedef shared_ptr<Node> node_ptr; 
    typedef tuple<int, int> loc;

    public:   
    RRA(GridWorld& g) : world(g){}

    int get_node_exp(){
        return node_exp;
    }

    int get_node_gen(){
        return node_gen;
    }

    int init(const State& start, const State& goal){
        node_ptr g (new Node(NULL, goal, 0, world.heuristic(start, goal)));
        open_list.push(g);
        inital_pos = start;
        return resume(start);
    }

    int resume(State n){
        while (!open_list.empty()){ 
            node_ptr cur(open_list.top());
            open_list.pop();
            cur->open = false;
            ++node_exp;
            
            if (cur->state.x == n.x && cur->state.y == n.y){
                return cur->g;
            }

            vector<State> children = world.successors(cur->state);
            node_gen += children.size();
            for (int i = children.size(); i > 0; --i){
                State child = children[i - 1];
                loc child_tup = make_tuple(child.x, child.y);
                auto it = node_map.find(child_tup);
                if (it == node_map.end()){
                    // new node discovered
                    node_ptr child_node (new Node(cur, child, cur->g + 1, world.heuristic(child, inital_pos)));
                    open_list.push(child_node);
                    node_map[child_tup] = child_node;
                } else {
                    // check to see if it is in the conceptual open list
                    node_ptr child_node = it->second;
                    if (child_node->open && cur->g < child_node->g){
                        child_node->parent = cur;
                        child_node->g = cur->g + 1;
                        child_node->state = child;
                        open_list.push(child_node);
                    }
                }
            }
        }

        return -1;
    }

    double abstract_dist(const State& n, const State& goal){

        loc child_tup = make_tuple(n.x, n.y);
        auto it = node_map.find(child_tup);
   
        if (it != node_map.end() && it->second->open == false){
            return it->second->g;
        }

        if (resume(n) != -1){
            it = node_map.find(child_tup);
            return it->second->g;
        }

        return numeric_limits<double>::infinity();
    }

	protected:
		GridWorld& world;
        priority_queue<node_ptr, vector<node_ptr>, Node::compare_f > open_list;
        unordered_map <loc, node_ptr, hash<loc>> node_map;
        State inital_pos;
        int node_exp = 0;
        int node_gen = 0;
};