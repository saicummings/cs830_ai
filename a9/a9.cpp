/*
 * CS830 Assignment 9
 * Sai Lekyang
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <limits>
#include <stdlib.h>
#include <unordered_map>
#include <algorithm>

using namespace std;

bool DEBUG = false;
double GAMMA = 0.9;
double THETA = 0.001;
int START_STATE = -1;
int NUMBER_OF_STATE = 0;
string ALGO = "";

vector <double> V;
vector <int> POLICY;
vector <bool> TERMINAL;
vector <double> REWARD;
vector<vector<unordered_map<int,double>>> ACTIONS;

void rtdp(){
    // vector <int> path;
    int trial = 0;
    int backup = 0;
    int cur_state;
    double v;
    
    V = REWARD;
    // for (int r = 0; r < REWARD.size(); ++r){
    //     V[r] = max(0.0, REWARD[r]);
    // }

    while(trial < THETA){
        cur_state = START_STATE;
        if(DEBUG) cout << "-- " << trial << " --" << endl;

        vector <int> path;
    
        while(!TERMINAL[cur_state]){
            path.push_back(cur_state);

            vector <double> Vprev = V;
            double best_a; 
            double vmax = numeric_limits<double>::lowest();
            // arg min on a 
            for (int a = 0; a < ACTIONS[cur_state].size(); ++a){
                v = 0;
                for (auto it : ACTIONS[cur_state][a]){
                    v += it.second * Vprev[it.first];
                }
                if(DEBUG) cout << a << " " << cur_state << " " << v << endl;
                
                v = REWARD[cur_state] + v; // * GAMMA;
                
                if(DEBUG) cout << "  " << v << endl;

                if(vmax < v) {
                    vmax = v;
                    best_a = a;
                }
            }   
            V[cur_state] = vmax;
            POLICY[cur_state] = best_a; 
            
            if(DEBUG) cout << best_a << " " << REWARD[cur_state]  << endl; 
            
            ++backup;
            // now pick among s' weighted by T(s,a,s')
            float p = 0;
            float r = (double) rand() / (RAND_MAX);
            for (auto it : ACTIONS[cur_state][best_a]){
                p += it.second;
                if (r < p){
                    cur_state = it.first;
                    break;
                }
            }
            if(DEBUG) cout << endl;
        }

        vector <double> Vprev = V;

        //  updates from tail
        
        for (int b = path.size() - 1; b > 0; --b){
            int prev = path[b - 1];
            double vmax = numeric_limits<double>::lowest();
            for (int sa = 0; sa < ACTIONS[prev].size(); ++sa){
                v = 0;
                for (auto it : ACTIONS[prev][sa]){
                    v += it.second * Vprev[it.first];
                }
                if(vmax < v) {
                    vmax = v;
                }
            }
            V[prev] = max(0.0, REWARD[prev]) + vmax * GAMMA; 
        }

        ++trial;
    } 

    for (int p = 0; p < POLICY.size(); ++p){
        if(TERMINAL[p] || POLICY[p] == -1)
            cout << endl;
        else
            cout << POLICY[p] << endl;
    }
    cout << backup << " backups performed." << endl;
    
}


void rtdp2(){
 // vector <int> path;
    int trial = 0;
    int backup = 0;
    int cur_state;
    double v;
    
    V = REWARD;
    // for (int r = 0; r < REWARD.size(); ++r){
    //     V[r] = REWARD[r];
    //     if(TERMINAL[r]) V[r] = 10 * V[r];
    // }

    while(trial < THETA){
        cur_state = START_STATE;
        if(DEBUG) cout << "-- " << trial << " --" << endl;

        vector <int> path;
    
        while(!TERMINAL[cur_state]){
            path.push_back(cur_state);

            vector <double> Vprev = V;
            double best_a; 
            double vmax = numeric_limits<double>::lowest();
            // arg min on a 
            for (int a = 0; a < ACTIONS[cur_state].size(); ++a){
                v = 0;
                for (auto it : ACTIONS[cur_state][a]){
                    v += it.second * Vprev[it.first];
                }
                if(DEBUG) cout << a << " " << cur_state << " " << v << endl;
                
                v = REWARD[cur_state] + v; // * GAMMA;
                
                if(DEBUG) cout << "  " << v << endl;

                if(vmax < v) {
                    vmax = v;
                    best_a = a;
                }
            }   
            V[cur_state] = vmax;
            POLICY[cur_state] = best_a; 
            
            if(DEBUG) cout << best_a << " " << REWARD[cur_state]  << endl; 
            
            ++backup;
            // now pick among s' weighted by T(s,a,s')
            float p = 0;
            float r = (double) rand() / (RAND_MAX);
            for (auto it : ACTIONS[cur_state][best_a]){
                p += it.second;
                if (r < p){
                    cur_state = it.first;
                    break;
                }
            }
            if(DEBUG) cout << endl;
        }

        ++trial;

        vector <double> Vprev = V;

        //  updates from tail
        for (int b = path.size() - 1; b > 0; --b){
            int prev = path[b - 1];
            v = 0;
            double vmax = numeric_limits<double>::lowest();
            for (int a = 0; a < ACTIONS[prev].size(); ++a){
                for (auto it : ACTIONS[prev][a]){
                    v += it.second * Vprev[it.first];
                }
                v = REWARD[prev] + v * GAMMA; 
                if(vmax < v) {
                    vmax = v;
                    V[prev] = v;
                }
            }
        }
    } 

    for (int p = 0; p < POLICY.size(); ++p){
        if(TERMINAL[p] || POLICY[p] == -1)
            cout << endl;
        else
            cout << POLICY[p] << endl;
    }
    cout << backup << " backups performed." << endl;
    
}

void rtdpworks(){
    // vector <int> path;
    int trial = 0;
    int backup = 0;
    int cur_state;
    double v;
    
    V = REWARD;
    // for (int r = 0; r < REWARD.size(); ++r){
    //     V[r] = max(0.0, REWARD[r]);
    // }

    while(trial < THETA){
        cur_state = START_STATE;
        if(DEBUG) cout << "-- " << trial << " --" << endl;

        vector <int> path;
    
        while(!TERMINAL[cur_state]){
            path.push_back(cur_state);

            vector <double> Vprev = V;
            double best_a; 
            double vmax = numeric_limits<double>::lowest();
            // arg min on a 
            for (int a = 0; a < ACTIONS[cur_state].size(); ++a){
                v = 0;
                for (auto it : ACTIONS[cur_state][a]){
                    v += it.second * Vprev[it.first];
                }
                if(DEBUG) cout << a << " " << cur_state << " " << v << endl;
                
                v = REWARD[cur_state] + v; // * GAMMA;
                
                if(DEBUG) cout << "  " << v << endl;

                if(vmax < v) {
                    vmax = v;
                    best_a = a;
                }
            }   
            V[cur_state] = vmax;
            POLICY[cur_state] = best_a; 
            
            if(DEBUG) cout << best_a << " " << REWARD[cur_state]  << endl; 
            
            ++backup;
            // now pick among s' weighted by T(s,a,s')
            float p = 0;
            float r = (double) rand() / (RAND_MAX);
            for (auto it : ACTIONS[cur_state][best_a]){
                p += it.second;
                if (r < p){
                    cur_state = it.first;
                    break;
                }
            }
            if(DEBUG) cout << endl;
        }

        vector <double> Vprev = V;

        //  updates from tail
        for (int b = path.size() - 1; b > 0; --b){
            int prev = path[b - 1];
            v = 0;
            for (auto it : ACTIONS[prev][POLICY[prev]]){
                v += it.second * Vprev[it.first];
            }
            
            V[prev] = REWARD[prev] + v * GAMMA; 
        }

        ++trial;
    } 

    for (int p = 0; p < POLICY.size(); ++p){
        if(TERMINAL[p] || POLICY[p] == -1)
            cout << endl;
        else
            cout << POLICY[p] << endl;
    }
    cout << backup << " backups performed." << endl;
    
}

void value_iteration(){
    int backup = 0;
    double v;
    int t = 0;
    while(1){
        vector <double> Vprev = V;
        double vmax = 0;
        for (int i = 0; i < V.size(); ++i){
            if (!TERMINAL[i])
                vmax = numeric_limits<double>::lowest();
            else
                V[i] = REWARD[i];
            for (int a = 0; a < ACTIONS[i].size(); ++a){
                v = 0;
                for (auto it : ACTIONS[i][a]){
                    v += it.second * Vprev[it.first];
                }
                v = REWARD[i] + GAMMA *v;
                if(vmax < v) {
                    vmax = v;
                    V[i] = v;
                    POLICY[i] = a;
                }
            }
            ++backup;
        } 

        if (DEBUG){
            for (double vv : V){
                cout << vv << endl;
            }
            cout << endl;    
        }
        double delta = 0;
        for (int i = 0; i < V.size(); ++i){
            double dif = abs(V[i] - Vprev[i]);
            if (dif > delta) delta = dif;
        }
        if (delta < THETA) break;
    }
    for (int p = 0; p < POLICY.size(); ++p){
        if(TERMINAL[p])
            cout << endl;
        else
            cout << POLICY[p] << endl;
    }
    cout << backup << " backups performed." << endl;
}

void read(){
    string line;
    int action_count = 0;
    int action_index = 0;
    while(getline(cin, line)){
        if (line[0] != '#'){
            stringstream ss(line);
            string word;
            ss >> word;
            if (word == "number"){
                ss >> word; 
                ss >> word;
                ss >> NUMBER_OF_STATE;
                V = vector<double>(NUMBER_OF_STATE, 0);
                POLICY = vector<int>(NUMBER_OF_STATE, -1);
            } else if (word == "start"){
                ss >> word; 
                ss >> START_STATE;
            } else if (action_count == 0) {
                REWARD.push_back(stod(word));
                ss >> word;
                TERMINAL.push_back(stoi(word));
                ss >> action_count;
                ACTIONS.push_back(vector<unordered_map<int,double>>(action_count));
                action_index = 0;
            } else {
                int successors = stoi(word);
                for(int i = 0; i < successors; ++i){
                    int dest_state;
                    ss >> dest_state;
                    double prob;
                    ss >> prob;
                    ACTIONS[REWARD.size() - 1][action_index][dest_state] += prob;
                }
                --action_count;
                ++action_index;
            }
        }
    }
}

int main(int argc, char** argv){

    ALGO = argv[1];
    GAMMA = stod(argv[2]);
    THETA = stod(argv[3]);

    for (int i = 0; i < argc; ++i){
        string input = argv[i];
        if (input == "-debug" ){
            DEBUG = true;
        } 
    }
    read();

    if (DEBUG) {   
        cout << "START STATE" << endl;
        cout << START_STATE << endl; 
        cout << "REWARDS" << endl;
        for (double r : REWARD){
            cout << r << " ";
        } cout << endl;

        cout << "TERMINAL" << endl;
        for (bool r : TERMINAL){
            cout << r << " ";
        } cout << endl;
    }
    srand(time(0) + THETA);
    if (ALGO == "vi")
        value_iteration();
    else if (ALGO == "rtdp")
        rtdp();

}
