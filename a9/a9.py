#!/usr/bin/env python3
import sys
import re
import numpy as np
from numpy import linalg as LA

ALGO = sys.argv[1]
GAMMA = float(sys.argv[2])
THETA = float(sys.argv[3])
NUMBER_OF_STATES = 0
READING_STATE = True
START_STATE = -1
V = []
REWARD = []
TEMRINAL = []
ACTIONS = []
STATE_COUNT = -1
ACTION_COUNT = 0
ACTION_READ = 0

for line in sys.stdin:
    if line[0] == '#':
        continue
    l = re.split(r'\s+',line)
    if l[0] == 'number':
        NUMBER_OF_STATES = int(l[3])
        V = np.zeros(shape=(NUMBER_OF_STATES, 1))
        REWARD = np.zeros(shape=(NUMBER_OF_STATES, 1))
    elif l[0] == 'start':
        START_STATE = int(l[2])
    elif ACTION_COUNT == 0:
        STATE_COUNT += 1
        ACTION_READ = 0
        REWARD[STATE_COUNT]=(float(l[0]))
        TEMRINAL.append(int(l[1]))
        ACTION_COUNT = int(l[2])
        while len(ACTIONS) < ACTION_COUNT:
            ACTIONS.append(np.zeros(shape=(NUMBER_OF_STATES, NUMBER_OF_STATES)))
    else:
        successors = int(l[0])
        for i in range(0, successors):
            dest_state = int(l[i*2 + 1])
            prob = float(l[i*2 + 2])
            ACTIONS[ACTION_READ][STATE_COUNT][dest_state] += prob
        ACTION_READ += 1
        ACTION_COUNT -= 1

POLICY = np.zeros(shape=(NUMBER_OF_STATES, 1))


if (ALGO == 'vi'):
    backup = 0
    while(True):
        Vprev = V.copy()
        deltamax = 0
        for i in range(0, len(V)):
            if TEMRINAL[i]:
                V[i] = REWARD[i]
            vmax = -float('inf')
            for a in range(0, len(ACTIONS)):
                # print(i, a)
                # print(Vprev[i])
                # print(ACTIONS[a][i])
                v = REWARD[i] + GAMMA *(ACTIONS[a][i] @ Vprev)
                if(vmax < v):
                    # print('>>' + str(v))
                    vmax = v
                    V[i] = v
                    POLICY[i] = a
                    backup += 1
                # print()
        deltamax = abs(LA.norm(V) - LA.norm(Vprev))
        # print(V)
        # print()
        if deltamax < THETA:
            break

    j = 0
    for p in POLICY:
        if TEMRINAL[j] == 0:
            print(int(p))
        else:
            print()
        j += 1
    print(str(backup) + ' backups performed.')
else:
    print()
