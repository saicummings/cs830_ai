/*
 * CS830 Assignment 10
 * Sai Lekyang
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <limits>
#include <stdlib.h>
#include <algorithm>
#include <math.h>   
#include <unordered_map>
#include <queue>

using namespace std;

bool DEBUG = false;
int KNN = 5;
double ALPHA = 0.000221;
string ALGO = "";


struct Train_knn{
    int label;
    vector<int> data;
};

// vector<vector<int>> TEST;
vector<Train_knn> TRAIN_KNN;
unordered_map<int,vector<double>> THETA;
vector<double> YHAT;
// vector<vector<vector<int>>> FREQ_TABLE;
unordered_map<int, unordered_map<int, unordered_map<int, int>>> FREQ_TABLE; // [att][clss][val]
unordered_map<int,int> FREQ;
int TRAIN_LEN = 0;

typedef pair<double, int> knn_pair; 

void read(){
    string line;
    int r = 0;
    int att;
    int val;
    int cls;
    int num;
    bool train = false;
    
    while(getline(cin, line)){
        stringstream ss(line);
        if (r == 0){
            string word;
            ss >> att;
            ss >> word;
            ss >> val;
            ss >> word;
            ss >> cls;
            ++r;

            if (ALGO == "linear"){
                for (int i = 0; i < cls; ++i){
                    vector<double> vec1(att + 1, 0.0);
                    THETA[i] = vec1;
                    vector<double> vec2(cls, 0.0);
                    YHAT = vec2;
                }
            } else if (ALGO == "nb"){

            }

        } else if (line[0] == '-'){
            train = !train;
        } else if(train){
            if (DEBUG) cout << "\n######## TRAINING ########" << endl;
            if (DEBUG) cout << line << endl;
            
            vector <int> train_list;
            
            if (ALGO == "knn"){
                ss >> num;
                Train_knn t = Train_knn();
                t.label = num;
                while(ss >> num){
                    train_list.push_back(num);
                }
                TRAIN_KNN.push_back(t);
                TRAIN_KNN[TRAIN_KNN.size() - 1].data = train_list;
                
            } else if (ALGO == "linear"){
                ss >> num;
                int y = num;

                while(ss >> num){
                    train_list.push_back(num);
                } 


                for (int i = 0; i < cls; ++i){
                    // update y hat
                    if (DEBUG) cout << "**Update Y HAT** \n  yhat = ";
                    
                    YHAT[i] = THETA[i][0];
                    if (DEBUG) cout << THETA[i][0];
                    for (int w = 0; w < train_list.size(); ++w){
                        YHAT[i] += THETA[i][w + 1] * train_list[w];
                        if (DEBUG) cout << " + " << THETA[i][w + 1] << "(" << train_list[w] << ")";
                        
                    } 
                    if (DEBUG)cout << "-> " << YHAT[i] << endl;


                    if (DEBUG) cout << "**Update THETA**" << endl;

                    if (DEBUG) cout << "class " << i << endl;
                    int yy;
                    if (i == y){
                        yy = 1;
                    } else {
                        yy = 0;
                    }

                    if (DEBUG) cout << "  theta0 <- " << THETA[i][0] << " - " << ALPHA <<"*"<< "(" << YHAT[i] <<"-"<<  yy <<")";
                    THETA[i][0] = THETA[i][0] - ALPHA * (YHAT[i] - yy);
                    if (DEBUG) cout << "->" <<  THETA[i][0] << endl;
                    for (int j = 0; j < att; ++j){
                        if (DEBUG) cout << "  theta" << j + 1 << " <- " << THETA[i][j + 1] << " - " << ALPHA <<"*"<< "(" << YHAT[i] <<"-"<<  yy <<")" << train_list[j];
                        THETA[i][j + 1] = THETA[i][j + 1] - ALPHA * (YHAT[i] - yy) * train_list[j];
                        if (DEBUG) cout << "->" <<  THETA[i][j + 1] << endl;
                    }

                }
                
            } else {
                // nb
                ss >> num;
                int c = num;
                FREQ[c] += 1;
                TRAIN_LEN++;
                int i = 0;
                while(ss >> num){
                    FREQ_TABLE[i++][c][num] += 1;
                } 
            }
        } else {
            if (DEBUG) cout << "\n######## TESTING ########" << endl;
            if (ALGO == "knn"){
                vector <int> test;
                while(ss >> num){
                    test.push_back(num);
                }

                priority_queue <knn_pair, vector<knn_pair>, greater<knn_pair> > min_heap;

                for(Train_knn train : TRAIN_KNN){
                    double dist = 0;
                    for (int i = 0; i < train.data.size(); ++i){
                        dist += (double)(train.data[i] - test[i]) * (double)(train.data[i] - test[i]);

                    }
 
                    dist = sqrt(dist / 3);
                    min_heap.push(make_pair(dist, train.label));
                }

                unordered_map<int, int> freq;
                unordered_map<int, double> dist;


                for (int i = 0; i < KNN; ++i){
                    knn_pair k = min_heap.top();
                    if (DEBUG) cout << "att=" << k.second << " dist=" << k.first << endl;
                    freq[k.second] += 1;
                    dist[k.second] += k.first;
                    min_heap.pop();
                    if (min_heap.empty()) break;
                }

                int best = 0;
                int max = 0;
                double closest = numeric_limits<double>::infinity();
                for (auto it : freq){

                    if (it.second >= max){

                        if (it.second > max || dist[it.first] < closest){
                            max = it.second;
                            best = it.first;
                            closest = dist[it.first];
                        } 
                    }
                }

                cout << best << " " << 1.0 * max / KNN << endl;

            } else if (ALGO == "linear"){
                vector <int> test_list;
                while(ss >> num){
                    test_list.push_back(num);
                }

                double conf = 0;
                double max = 0;
                int best = 0;

                for (int i = 0; i < cls; ++i){
                    if (DEBUG) cout << "class" << i << endl;
                    // check each class
                    double value = THETA[i][0];
                    int t = 1;

                    if (DEBUG) cout << value;
                    for (int t = 0; t < test_list.size(); ++t){
                        if (DEBUG) cout << " + " << THETA[i][t + 1] << "(" << test_list[t] << ")";
                        value += THETA[i][t + 1] * test_list[t];
                    }
                    if (DEBUG) cout <<  "=>" << value << endl;

                    if (max < value){
                        max = value;
                        best = i;
                    }

                    conf += value;
                }
                if (DEBUG) cout <<"BEST : "; 
                cout << best << " " << max/conf << endl;
            } else {
                double max = 0;
                int best = 0;
                vector <int> test_list;
                while(ss >> num){
                    test_list.push_back(num);
                }

                for (int i = 0; i < cls; ++i){
                    if(DEBUG) cout << "clss" << i << endl; 
                    double freq = 0;
                    if (FREQ.find(i) != FREQ.end()){
                        freq += FREQ[i];
                    }
                    double h = freq / (TRAIN_LEN);
                    for (int j = 0; j < att; ++j){
                        double valfreq_nominator = 0.1;
                        if (FREQ_TABLE.find(j) != FREQ_TABLE.end()){
                            if (FREQ_TABLE[j].find(i) != FREQ_TABLE[j].end()){
                                if (FREQ_TABLE[j][i].find(test_list[j]) != FREQ_TABLE[j][i].end()){
                                    valfreq_nominator += FREQ_TABLE[j][i][test_list[j]];
                                }
                            }
                        }
                        if(DEBUG) cout << valfreq_nominator << " ";
                        // valfreq_nominator += att;
                        
                        double valfreq_denominator = TRAIN_LEN ;
                        h *= (valfreq_nominator/valfreq_denominator);
                    }
                    if(DEBUG) cout << endl;

                    if (max < h){
                        max = h;
                        best = i;
                    }
                }    
                cout << best << " " << ((double)FREQ[best])/TRAIN_LEN << endl;

            }
        }
    }
}

int main(int argc, char** argv){

    for (int i = 0; i < argc; ++i){
        string input = argv[i];
        if (input == "-debug" ){
            DEBUG = true;
        } 
    }

    ALGO = argv[1];
    read();
    
    if (ALGO == "knn"){

        // knn();
    }
    else if (ALGO == "nb" && DEBUG){
        for (auto it1: FREQ_TABLE){
            cout << "att " <<  it1.first << endl;
            for (auto it2 : it1.second){
                cout << "  cls " << it2.first << "\n    ";
                for (auto it3 : it2.second){
                    cout << it3.first << ":" << it3.second << " ";
                    
                }
                cout << endl;
            }
        }
    }
}
