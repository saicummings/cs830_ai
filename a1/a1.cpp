/*
 * CS830 Assignment 1
 * Sai Lekyang
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <stack>
#include <list>
#include <queue>
#include <functional>

using namespace std;
bool DEBUG = false;
char ALGO;
int GRID_R;
int GRID_C;
int N_GEN = 0;
int N_EXP = 0;
int MAX_CHARGE;
int DUST;
int ID = 0;
bool BATTERY_FLAG = false;
char ORDER[6] = {'N', 'E', 'S', 'W', 'V', 'R'}; 

struct Node{
    string state;
    int x, y, vac, cost, charge;
    bool overdust;
    bool overcharge;
    Node* parent;
    char move;
    int id;
    Node(string state, int x, int y): 
        state(state), x(x), y(y), overdust(false), overcharge(false), parent(NULL), move(0){}
};

// 0,0 is top left corner
int get1D(int x, int y){
    return x + GRID_C * y;
}

bool checkGoal(Node* cur){
    if (DUST != cur->vac){
        return false;
    }
    stack<char>path;
    while(cur->parent != NULL){
        path.push(cur->move);
        cur = cur->parent;
    }

    while(!path.empty()){
        cout << path.top() << endl;
        path.pop();
    }

    cout << N_GEN << " nodes generated" << endl;
    cout << N_EXP << " nodes expanded" << endl;

    return true;
}

void d1(char c, Node* n){
    cout << c << " id:" << n->id << " mv:" << n->move << " vc:" << n->vac << " ch:" << n->charge << endl;
    cout << n->state << endl;
}

void d2(string s){
    for (int i = 0; i < GRID_C * GRID_R; ++i){
        cout << s[i];
        if ((i + 1) % GRID_C == 0){
            cout << endl;
        }
    }
}

void debug(Node* cur){
    if (cur->parent){
        d1('P', cur->parent);
        d2(cur->parent->state);
    }
    d1('C', cur);
    d2(cur->state);
    cout << endl;
}

Node* getChild(Node* p, char dir, unordered_map<string, Node*>& closed_list){
    int x = p->x;
    int y = p->y;
    int prev1D = get1D(x, y);

    // If no battery flag, force vaccum if over dust
    if (!BATTERY_FLAG && p->overdust && dir != 'V'){
        if(DEBUG) cout << "1" << endl;
        return NULL;
    }

    // Checking if there's enough juice
    if (BATTERY_FLAG && dir != 'R' && p->charge == 0){
         if(DEBUG) cout << "2" << endl;
        return NULL;
    }

    // Check if move is possible
    if (dir == 'N'){
        if (p->y == 0){
             if(DEBUG) cout << "3" << endl;
            return NULL;
        }
        --y;
    } else if (dir == 'E'){
        if (p->x + 1 == GRID_C){
             if(DEBUG) cout << "4" << endl;
            return NULL;
        }
        ++x;
    } else if (dir == 'S'){
        if (p->y + 1 == GRID_R){
             if(DEBUG) cout << "5" << endl;
            return NULL;
        }
        ++y;
    } else if (dir == 'W'){
        if (p->x == 0){
             if(DEBUG) cout << "6" << endl;
            return NULL;
        }
        --x;
    } else if (dir == 'V'){
        if (!p->overdust){
             if(DEBUG) cout << "7" << endl;
            return NULL;
        }
    } else if (dir == 'R'){
        if (!BATTERY_FLAG || !p->overcharge || p->charge == MAX_CHARGE){
             if(DEBUG) cout << "8" << endl;
            return NULL;
        }
    }

    int next1D = get1D(x, y);
    
    // Obstacle at next step
    if( p->state[next1D] == '#'){
         if(DEBUG) cout << "9" << endl;
        return NULL;
    }

    // Generate next grid state
    ++N_GEN;
    string childState = p->state;
    childState[next1D] = '@';
    bool overdust = false;
    bool overcharge = false;
    int charge = p->charge;
    int vac = p->vac;

    if (dir == 'V'){
        --charge;
        ++vac;
    } else if (dir =='R'){
        charge = MAX_CHARGE;
        // childState[next1D] = 'R';
        overcharge = true;
    } else {
        if (p->overdust){
            childState[prev1D] = '*';
        } else if (p->overcharge){
            childState[prev1D] = ':';
        } else {
            childState[prev1D] = '_';        
        }

        if (p->state[next1D] == '*'){
            overdust = true;
            childState[next1D] = '!';
        } else if (p->state[next1D] == ':'){
            overcharge = true;
            // childState[next1D] = '+';
        }

        if (BATTERY_FLAG){
            --charge;
            if (charge == 0){
                childState[next1D] = 'x';
            }
        }
    }

    if (BATTERY_FLAG && closed_list.count(childState + to_string(charge)) > 0){
        if(DEBUG) {
            cout << "9" << endl;
            cout << childState + to_string(charge)<< endl;
        }
        return NULL;
    } else if (closed_list.count(childState) > 0) {
        if(DEBUG) {
            cout << "10" << endl;
            cout << childState << endl;
        }
        return NULL;
    }
        
    Node* child = new Node(childState, x, y);
    child->vac = vac;
    child->parent = p;
    child->move = dir;
    child->cost =  p->cost + 1;
    child->id = ID++;
    child->charge =  charge;
    child->overdust = overdust;
    child->overcharge = overcharge;
    return child;
}

class compareSteps {
// Min step
public:
    bool operator()(Node* n1, Node* n2) {
        return n1->cost > n2->cost;
    }
};

void printNoSol(){
    cout << "No solution found." << endl;
    cout << N_GEN << " nodes generated" << endl;
    cout << N_EXP << " nodes expanded" << endl;
}

void UniformCost(Node* n){
    priority_queue<Node*, vector<Node*>, compareSteps> min_heap;
    unordered_map<string, Node*> closed_list;
    min_heap.push(n);
    if (BATTERY_FLAG){
        closed_list[n->state + to_string(n->charge)] = n;
    } else {
        closed_list[n->state] = n;                    
    }

    while(!min_heap.empty()){
        Node* cur = min_heap.top();
        min_heap.pop();
        ++N_EXP;

        if (DEBUG) debug(cur);

        if (checkGoal(cur)){
            return;
        }

        for(char dir : ORDER){
            Node* n = getChild(cur, dir, closed_list);
            if(n != NULL){
                min_heap.push(n);
                if (BATTERY_FLAG){
                    closed_list[n->state + to_string(n->charge)] = n;
                } else {
                    closed_list[n->state] = n;                    
                }
            }
        }
    }
    printNoSol();
}

void DFS(Node* n){
    list<Node*> open_list;
    unordered_map<string, Node*> closed_list;
    open_list.push_front(n);
    if (BATTERY_FLAG){
        closed_list[n->state + to_string(n->charge)] = n;
    } else {
        closed_list[n->state] = n;                    
    }

    while (!open_list.empty()){
        Node* cur = open_list.front();
        open_list.pop_front();
        ++N_EXP;

        if (DEBUG) debug(cur);

        if (checkGoal(cur)){
            return;
        }

        int children = 0;

        for(char dir : ORDER){
            Node* n = getChild(cur, dir, closed_list);
            if(n != NULL){
                ++children;
                open_list.push_front(n);
            }
        }

        // Dead end
        if (children == 0){
            if (BATTERY_FLAG){
                closed_list.erase(cur->state + to_string(cur->charge));
            } else {
                closed_list.erase(cur->state);
            }
        } else {
            if (BATTERY_FLAG){
                closed_list[cur->state + to_string(n->charge)] = cur;
            } else {
                closed_list[cur->state] = cur;                    
            }
        }
    }
    printNoSol();
}

int main(int argc, char** argv){
    int start1D;
    
    if (argc < 2){
        cout << "Usage: ./executable <alg> " << endl;
        exit(0);
    } 

    string dim;
    getline (cin, dim);
    GRID_C = stoi(dim);
    getline (cin, dim);
    GRID_R = stoi(dim);
    MAX_CHARGE = GRID_C * 2 + GRID_R * 2 - 4;
    string grid = "";
    
    for (int i = 0; i < GRID_C * GRID_R; ++i){
        char out = getchar();

        if (out == '@'){
            start1D = i;
        } else if (out == '*'){
            ++DUST;
        }
        grid += out;
        
        if ( i % GRID_C == GRID_C - 1){
            getchar();
        }
        
    }
    
    string start_state(grid);

    Node n = Node(start_state, start1D % GRID_C, start1D / GRID_C);
    n.parent = NULL;
    n.cost = 0;
    n.vac = 0;
    n.id = ID++;
    n.charge = MAX_CHARGE;

    string arg1 = argv[1];

    if (argc == 3){
        string arg2 = argv[2];
        if (arg2 == "-battery"){
            BATTERY_FLAG = true;

        }
    }

    if (arg1 == "d" || arg1 == "depth-first"){
        ALGO = 'D';
        DFS(&n);
    } else if (arg1 == "u" || arg1 == "uniform-cost"){
        ALGO = 'U';
        UniformCost(&n);
    } else {
        cout << "Unknown algorithm: " << arg1 << endl;
    }
}

