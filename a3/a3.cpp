#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <stack>
#include <list>
#include <queue>
#include <functional>
#include <math.h> 

using namespace std;

struct Node{
    int id;
    int parent;
    double x;
    double y;
    double dx;
    double dy;
    double r; // counter clock-wise
    double s; // magnitude
    Node(int id, int p, double x, double y, double dx, double dy, double r, double s):
        id(id), parent(p), x(x), y(y), dx(dx), dy(dy), r(r), s(s){};
};

int ID = -1;
int GRID_R;
int GRID_C;
double START_X;
double START_Y;
double GOAL_X;
double GOAL_Y;
vector <pair<int,int>> obstacles;
vector <Node> tree;

double distance(double x1, double y1, double x2, double y2) 
{ 
    return sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2)); 
} 


bool inObstacle(double x, double y){
    if (x <= 0 || x >= GRID_C )
        return true;

    if (y <= 0 || y >= GRID_R)
        return true;  

    for (int i = 0; i < obstacles.size(); ++i){
        double ox = obstacles[i].first;
        double oy = obstacles[i].second;
        if(ox <= x && x <= ox + 1 && oy >= y && y >= oy - 1){
            return true;
        }
    }
    return false;
}

void getRandom(double& x, double& y){
    if ( rand() % 20 == 0 ){
        x = GOAL_X;
        y = GOAL_Y;
    } else {
        x = 0.025 + (GRID_C - 0.05) * ((double) rand() / (RAND_MAX));
        y = 0.025 + (GRID_R - 0.05) * ((double) rand() / (RAND_MAX));
    }
}

bool isClearLOS(double x, double y, double tx, double ty, double shortest_dist){
    // Check if there is a valid line of sight
    double v_x = tx - x;
    double v_y = ty - y;
    v_x *= (0.05/shortest_dist);
    v_y *= (0.05/shortest_dist);
    double trace_x = x;
    double trace_y = y;
    while(distance(trace_x, trace_y, tx, ty) > 0.05){
        trace_x += v_x;
        trace_y += v_y;
        if (inObstacle(trace_x, trace_y))
            return false;
    }
    return true;
}

bool isClearTraj(double& cx, double& cy, double& dx, double& dy, double r, double s){
    // Calculate trajectory and check for obstacle
    double t = 1/20.0;

    for (int i = 0; i < 20; ++i){
        double r_total = atan2(dy, dx) + r;
        dx = dx + (t * s * cos(r_total));
        dy = dy + (t * s * sin(r_total));
        cx += t * dx;
        cy += t * dy;

        if (inObstacle(cx, cy)){
            return false;
        }
    }

    return true;
}


void rrt(){
    double x, y;

    tree.push_back(Node(++ID, -1, START_X, START_Y, 0, 0, 0, 0));

    while(true){
        // Sample random point for target
        getRandom(x, y);
        while(inObstacle(x, y)){
            getRandom(x, y);
        }

        // Find the nearest vertext
        int shortest_indx = 0;
        double shortest_dist = distance(START_X, START_Y, x, y);
        for (int i = 1; i < tree.size(); ++i){
            double new_distance = distance(tree[i].x, tree[i].y, x, y);
            if (new_distance < shortest_dist){
                shortest_indx = i;
                shortest_dist = new_distance;
            }
        }
        

        Node p = tree[shortest_indx];

        if (distance(p.x, p.y, x, y) > 0.1 && isClearLOS(x, y, p.x, p.y, shortest_dist)){
            vector<Node> temp_nodes;

            // Create 10 random contols and trajectories to the target
            for (int i = 0; i < 10; ++i){
                double r = 2 * M_PI * ((double) rand() / (RAND_MAX));
                double s = 0.5 * ((double) rand() / (RAND_MAX));
                double cx = p.x;
                double cy = p.y;
                double dx = p.dx;
                double dy = p.dy;
                if( isClearTraj(cx, cy, dx, dy, r, s)){
                    temp_nodes.push_back(Node(-1, -1, cx, cy, dx, dy, r, s));
                }
            }


            // Take the closest traj to the target and add to tree
            if (temp_nodes.size() > 0){
                int s = 0;
                double bst = distance(temp_nodes[0].x, temp_nodes[0].y, x, y);
                for (int j = 1; j < temp_nodes.size(); ++j){
                    double d = distance(temp_nodes[j].x, temp_nodes[j].y, x, y);
                    if (d < bst){
                        s = j;
                        bst = d;
                    }
                }

    
                Node close = temp_nodes[s];
                tree.push_back(Node(++ID, shortest_indx, close.x, close.y, close.dx, close.dy, close.r, close.s));

                if (distance(GOAL_X, GOAL_Y, close.x, close.y) <= 0.1){
                    break;
                }
                    
            }
        }

    }
    stack<Node> trajectory;

    Node cur = tree[ID];

    while(cur.parent != -1){
        trajectory.push(cur);
        cur = tree[cur.parent];
    }

    cout << trajectory.size() << endl;
    while( !trajectory.empty()){
        Node c = trajectory.top();
        Node par = tree[c.parent];
        cout << par.x << " ";
        cout << par.y << " ";
        cout << par.dx << " ";
        cout << par.dy << " ";
        cout << c.r << " ";
        cout << c.s;
        cout << endl;
        trajectory.pop();
    }

    cout << tree.size() - 1 << endl;
    for (int i = 1; i < tree.size(); ++i){
        Node c = tree[i];
        Node par = tree[c.parent];
        cout << par.x << " ";
        cout << par.y << " ";
        // cout << c.x << " ";
        // cout << c.y << " ";
        cout << par.dx << " ";
        cout << par.dy << " ";
        cout << c.r << " ";
        cout << c.s;
        cout << endl;
    }
}

int main(int argc, char** argv){
    int start1D;    
    string dim;
    getline (cin, dim);
    GRID_C = stoi(dim);
    getline (cin, dim);
    GRID_R = stoi(dim);
    
    // Loop through individual cell from the input grid
    for (int i = 0; i < GRID_R; ++i){
        for (int j = 0; j < GRID_C; ++j){
           char g = getchar();
            if (g == '#'){
                obstacles.push_back(make_pair(j, GRID_R - i));
            }
        }
        getchar();
    }

    getline (cin, dim);
    START_X = stof(dim);
    getline (cin, dim);
    START_Y = stof(dim);
    getline (cin, dim);
    GOAL_X = stof(dim);
    getline (cin, dim);
    GOAL_Y = stof(dim);   

    rrt(); 
}

